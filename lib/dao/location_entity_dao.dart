import 'package:floor/floor.dart';
import 'package:postmanug/entity/location_entity.dart';

@dao
abstract class LocationEntityDao {
  @Query('SELECT * FROM LocationEntity')
  Future<List<LocationEntity>> findAllLocations();

  @Query('SELECT * FROM LocationEntity WHERE id = :id')
  Stream<LocationEntity> findLocationById(int id);

  @insert
  Future<void> insertLocation(LocationEntity locationEntity);

  @update
  Future<void> updateLocation(LocationEntity locationEntity);
}