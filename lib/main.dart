import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:location/location.dart';
import 'package:postmanug/entity/location_entity.dart';
import 'package:postmanug/pages/Login.dart';
import 'package:postmanug/pages/account.dart';
import 'package:postmanug/pages/add-business-profile.dart';
import 'package:postmanug/pages/add-individual-profile.dart';
import 'package:postmanug/pages/generate-address-code_new.dart';
import 'package:postmanug/pages/get-destination-address.dart';
import 'package:postmanug/pages/home.dart';
import 'package:postmanug/pages/input-verification-code.dart';
import 'package:postmanug/pages/onboarding.dart';
import 'package:postmanug/pages/package-details.dart';
import 'package:postmanug/pages/settings.dart';
import 'package:postmanug/pages/verify-number.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';

import 'database.dart';
import 'pages/generate-address-code.dart';
import 'pages/mail-history.dart';
import 'pages/notifications-screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Postman Client',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Postman Client'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Location _locationService  = new Location();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    new Future.delayed(
        const Duration(seconds: 1),
            () async {

//          check if user is logged in
            String logged = await Util.getPrefString(Util.USER_LOGGED_IN);
            if(logged!=null){
              if(logged == "true"){
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>
                      HomePage()
                  ),
                );
              }else{
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>
                      Login()
                  ),
                );
              }
            }else{
              Navigator.pop(context);
//              Navigator.push(
//                context,
//                MaterialPageRoute(builder: (context) =>
//                    OnBoarding()
//                ),
//              );

              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>
                    Login()
                ),
              );
            }


            /*Navigator.push(
              context,
              MaterialPageRoute(builder: (context) =>
                  AddBusinessProfileScreen()
              ),
            );*/

            /*var loc_item = {
              'latitude':0,
              'longitude':0,
              'post_office_box	':"p.o box 10",
              'plot_number	':"10",
              'physical_address':"tororo",
              'national_post_code':"11200",
              'street':"bazar street",
              'account_type':1,

            };
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => GenerateAddressCodeNew(
                    item:loc_item
                )));*/
            }


    );

    _registerListener();
  }
  static void backgroundCallback(List<LocationData> locations) async {
//    print('Sample app received data location: $locations');

    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings);

    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'location_update', 'Location Updates', 'You will receive location updates here',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//    await flutterLocalNotificationsPlugin.show(
//        0, 'New Location Received !', "${locations[0].latitude}, ${locations[0].longitude}", platformChannelSpecifics);

    final database = await $FloorAppDatabase.databaseBuilder(Util.DB_NAME).build();

    var locs = await database.locationEntityDao.findAllLocations();
//    print(locs.length);
    if(locs.length>0){
      LocationEntity loc = locs.elementAt(0);
      loc.latitude=locations[0].latitude;
      loc.longitude=locations[0].longitude;


    }else{
       LocationEntity loc = new LocationEntity(1, locations[0].latitude,locations[0].longitude);
       await database.locationEntityDao.insertLocation(loc);
    }





  }

  void _registerListener() async {
    bool _permission = await _locationService.requestPermission();
    print("Permission: $_permission");
    if (_permission) {
      bool statusBackgroundLocation = await _locationService.registerBackgroundLocation(backgroundCallback);
      print("statusBackgroundLocation: $statusBackgroundLocation");
    } else {
      print("Permission denied");
    }

  }

  void _removeListener() async {
    await _locationService.removeBackgroundLocation();
  }
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      backgroundColor: MyTheme.primaryColor,


      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent
        child: Image.asset(
          "assets/images/app_icon.png",
          width: 100,
          height: 100,
        ),
      ),
       // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
