import 'package:flutter/material.dart';

class MyTheme{

  static var milanoRed =  Color(0xFFCD1500);
  static var lightCorl =  Color(0xFFf7dfdc);
  static var black =  Color(0xFF000000);
  static var shipGray =  Color(0xFF505050);
  static var mercury =  Color(0xFFE6E5E5);
  static var seaShell =  Color(0xFFF1F1F1);
  static var primaryColor =  milanoRed;
  static var primaryColorDark =  lightCorl;
  static var colorAccent =  shipGray;
  static var brownColor =  Color(0xFFa37a4f);
  static var selectionColor =  Color(0xFFFB893C);
//  static var brownish = Color(0xFFEDE0BB);
  static const brownish =  Color(0xFFF4E9CD);
  static const light_grey =  Color(0xFFe0e0dc);
//  static const color = const Color(0xFFB74093);



  static const heading1 = 22.0;
  static const heading2 = 20.0;
  static const heading3 = 18.0;
  static const heading4 = 17.0;


  static const body1 = 18.0;
  static const body2 = 17.0;
  static const body3 = 16.0;
  static const body4 = 15.0;


  static Color color1 = HexColor("b74093");

  static var myTheme =  new ThemeData(
    primaryColor: primaryColor,
    primaryColorDark: primaryColorDark,
    accentColor: colorAccent,
  );

  static var text_style_bold = TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 16);
  static var text_style_bold_big = TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20);

  static var  dropDownLabelStyle =
  TextStyle(color: Colors.black, fontSize: 16.0);
  static var  dropDownMenuItemStyle =
  TextStyle(color: Colors.black, fontSize: 16.0);

}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

