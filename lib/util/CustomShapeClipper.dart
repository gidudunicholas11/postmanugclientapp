import 'package:flutter/material.dart';

class CustomShapeClipper extends CustomClipper<Path>{
  @override
  Path getClip(Size size) {
    // TODO: implement getClip
    final Path path = Path();
    path.lineTo(0.0, size.height/2);
    /*var firstEndPoint = Offset(0,
    (size.height/2)
    );*/

    /*var firstControlPoint = Offset(size.width*.5,
        (0)
    );*/

    var secondEndPoint = Offset(size.width/2,
        (size.height )
    );

    var thirdEndPoint = Offset(size.width,
        (size.height/2 )
    );
    /*var secondControlPoint = Offset(size.width*.75,
        (size.height - size.height*.125 *2)
    );*/

    path.quadraticBezierTo(secondEndPoint.dx, secondEndPoint.dy, thirdEndPoint.dx, thirdEndPoint.dy);
//    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy, secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, 0.0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }

}



class CustomShapeClipper2 extends CustomClipper<Path>{
  @override
  Path getClip(Size size) {
    // TODO: implement getClip
    final Path path = Path();
    path.lineTo(0.0, size.height/2);
    /*var firstEndPoint = Offset(0,
    (size.height/2)
    );*/

    /*var firstControlPoint = Offset(size.width*.5,
        (0)
    );*/

    var secondEndPoint = Offset(size.width/2,
        (size.height )
    );

    var thirdEndPoint = Offset(size.width,
        (size.height/2 )
    );
    /*var secondControlPoint = Offset(size.width*.75,
        (size.height - size.height*.125 *2)
    );*/

    path.quadraticBezierTo(secondEndPoint.dx, secondEndPoint.dy, thirdEndPoint.dx, thirdEndPoint.dy);
//    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy, secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, 0.0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }

}