

import 'package:built_value/json_object.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class Util {
  static var CITY_NAME_PREF = "pref_city_name";
  static var FROM_CURRENCY_PREF = "from_currency";
  static var TO_CURRENCY_PREF = "to_currency";
  static var LOCALE_PREF = "locale";
  static var LAST_LOGIN_USER = "pref_last_login_user";

  static String ACCOUNT_TYPE_PREF = "pref_account_type";

  static String ACCOUNT_PHONE_PREF = "pref_account_phone";

  static String REG_ID = "pref_reg_id";

  static String USER_TOKEN = "pref_user_token";

  static String USER_LOGGED_IN = "pref_user_logged";

  static String CLIENT_ID = "pref_client_id";

  static String LAST_SEARCH = "pref_last_search";

  static String MY_ADDRESSES = "pref_my_addresses";

  static String PKXNUM = "pref_pkxnum";

  static var DB_NAME = "app_database.db";



  static Future<Locale> getLocale() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    return  Locale(prefs.getString(LOCALE_PREF) ?? "en");
  }

  static Future<Locale> setLocale(Locale locale) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (locale!=null) {
      prefs.setString(LOCALE_PREF, locale.languageCode);
    }else{
    }
    return locale;
  }

  static Future<String> savePrefString(String key, String str) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (key!=null && str !=null) {
      prefs.setString(key, str);


    }else{
    }

    return str;
  }


  static Future<String> saveListPrefString(String key, String str) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (key!=null && str !=null) {
      prefs.setString(key, str);


    }else{
    }

    return str;
  }


  static Future<String> saveAddListToListPrefString(String key, String str) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (key!=null && str !=null) {

      String st = prefs.getString(key);
      List l = List();
      if(st!=null){
        l = json.decode(st);
      }

      List l2 = json.decode(str);
      var newList = new List.from(l)..addAll(l2);
      prefs.setString(key, json.encode(newList));


    }else{
    }

    return str;
  }

  static Future<String> saveAddItemToListPrefString(String key, String str) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (key!=null && str !=null) {
      String st = prefs.getString(key);
      List l = List();
      if(st!=null){
        l = json.decode(st);
      }

      var l2 = json.decode(str);
      l.add(l2);
      prefs.setString(key, json.encode(l));


    }else{
    }

    return str;
  }


  static Future<String> saveEditItemInListPrefString(String key, var item, String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (key!=null && item !=null) {

      String st = prefs.getString(key);
      List l = List();
      if(st!=null){
        l = json.decode(st);
      }

//      inde
      for (var i = 0; i < l.length; i++) {
        var a = l.elementAt(i);
        if(a[id]==item[id]){
          l.removeAt(i);
          l.insert(i, item);


          break;
        }
      }

     /* List l2 = json.decode(str);
      var newList = new List.from(l)..addAll(l2);
      prefs.setString(key, json.encode(newList));*/


    }else{
    }

    return null;
  }


  static Future<String> getPrefString(String key) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String st = prefs.getString(key);
//    print("returning "+st);

    return  st;
  }

  static Future<Map<String, dynamic>> getPrefStringJSON(String key) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String st = prefs.getString(key);
//    print("ststst "+st);
    Map<String, dynamic> user = json.decode(st);

    return  user;

  }

  static void snack(BuildContext context, String s) {

    Scaffold
        .of(context)
        .showSnackBar(SnackBar(content: Text(s)));

  }

  static void basicAlert(BuildContext context, String s) {
    Alert(context: context, title: "Notification", desc: s).show();

  }

  static Future<List> getLastSearch() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String st = prefs.getString(Util.LAST_SEARCH);

    try{
      return (json.decode(st) as List);
    }on Exception catch(_){
      return new List();
    }
  }

  static Future<List> getPrefList(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String st = prefs.getString(key);
    if(st!=null){
      return json.decode(st);
    }

    return  null;
  }


}

