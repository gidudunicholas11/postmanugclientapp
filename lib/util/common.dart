import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class MyColors {
  static const black = Color(0xff060518);
  static const white = Color(0xffffffff);
  static const blue = Color(0xff0029FF);
  static const gray = Color(0xffAAAABF);
  static const darkGray = Color(0xff9494A3);
  static const darkSlateBlue = Color(0xff493E83);
}

class Fonts {
  static const String AirbnbCereal = 'AirbnbCereal';
  static const String MyriadPro = 'MyriadPro';
  static const String BrownExtraBold = 'BrownExtraBold';
}

class TextStyles {
  static const airbnbCerealMedium = TextStyle(color: MyColors.white, fontWeight: FontWeight.bold, fontFamily: Fonts.MyriadPro);
  static const airbnbCerealBook = TextStyle(color: MyColors.white, fontWeight: FontWeight.normal, fontFamily: Fonts.MyriadPro);
  static const brownExtraBoldRegular = TextStyle(color: MyColors.black, fontWeight: FontWeight.normal, fontFamily: Fonts.BrownExtraBold);
}




class Utils{


   static void showSnackBar(String str, BuildContext context) {
    final snackBar = SnackBar(
      duration: Duration(seconds: 5),
      content: Container(
          height: 80.0,
          child: Center(
            child: Text(
              str,
              style: TextStyle(fontSize: 25.0),
            ),
          )),
      backgroundColor: Colors.greenAccent,
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }


}