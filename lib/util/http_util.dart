import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:postmanug/util/prefs.dart';

import 'package:shared_preferences/shared_preferences.dart';

class HttpUtil{
  static Future<String> get(var url, {bool with_auth=false}) async {
    var headers = {
      'Content-Type': 'application/json',
    'Accept': 'application/json',

    };

    if(with_auth){
      String token = await Util.getPrefString(Util.USER_TOKEN);
      headers['Authorization'] = 'JWT $token';
    }

    var response = await http.get(url, headers: headers);
    return response.body;
  }

  /*Future<String> delete(var url, {bool with_auth=false}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',

    };

    if(with_auth){
      Map<String,dynamic> params = await Util.getPrefStringJSON(Util.LAST_LOGIN_USER);
      String token = params['token'];
      headers['Authorization'] = 'Token $token';
    }

    var response = await http.delete(url, headers: headers);
    return response.body;
  }*/




  static Future<String> post(var url, var body, {bool with_auth=false}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',

    };

    if(with_auth){
//      Map<String,dynamic> params = await Util.getPrefStringJSON(Util.LAST_LOGIN_USER);
      String token = await Util.getPrefString(Util.USER_TOKEN);
//      print(token);
      headers['Authorization'] = 'JWT $token';
    }

    var response = await http.post(url, body: json.encode(body), headers: headers);
    return response.body;
  }


  static Future<String> put(var url, var body, {bool with_auth=false}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',

    };

    if(with_auth){
      Map<String,dynamic> params = await Util.getPrefStringJSON(Util.LAST_LOGIN_USER);
      String token = await Util.getPrefString(Util.USER_TOKEN);
      headers['Authorization'] = 'JWT $token';
    }

    var response = await http.put(url, body: json.encode(body), headers: headers);
    return response.body;
  }


  static  Future<String> patch(var url, var body, {bool with_auth=false}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',

    };

    if(with_auth){
      Map<String,dynamic> params = await Util.getPrefStringJSON(Util.LAST_LOGIN_USER);
      String token = await Util.getPrefString(Util.USER_TOKEN);
      headers['Authorization'] = 'JWT $token';
    }

    var response = await http.patch(url, body: json.encode(body), headers: headers);
    return response.body;
  }



}