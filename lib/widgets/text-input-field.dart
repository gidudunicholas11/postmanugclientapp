import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/theme.dart';

class MyTextInputField extends StatefulWidget{

  int minLine = 1;
  int maxLine =1;
  String hint = "";
  String Label = "";
  bool show_label = true;
  dynamic validator;

  TextEditingController controller;

  MyTextInputField({Key key, this.minLine, this.maxLine,this.hint, this.show_label, this.Label,this.controller, this.validator});


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new MyTextInputFieldState();
  }



}

class MyTextInputFieldState extends State<MyTextInputField>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[

        Align(
          alignment: Alignment.topLeft,
          child: Padding(padding: EdgeInsets.only(left: 16),
          child: (widget.show_label?Text(
            widget.Label,
            textAlign: TextAlign.left,
            style: TextStyles.airbnbCerealBook.copyWith(
                color: Colors.black,
                fontSize: MyTheme.heading4
            ),
          ):Container()),
          ),
        ),



        Container(
          child: Padding(
              padding: const EdgeInsets.all(5.0),
          child: TextFormField(
          minLines: widget.minLine,
          maxLines: widget.maxLine,
          autocorrect: false,
          controller: widget.controller,
          validator: widget.validator,



          decoration: InputDecoration(
          hintText: widget.hint,
          filled: true,
          fillColor: Color(0xFFDBEDFF),
          enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          borderSide: BorderSide(color: Colors.grey),
          ),
          ),
          ),
          ),
        )
      ],
    );
  }

}