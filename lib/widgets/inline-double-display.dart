import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/theme.dart';

class InlineDoubleDisplay extends StatelessWidget{
  final String name_text;
  final String value_text;

  const InlineDoubleDisplay({Key key, this.name_text, this.value_text}) : super(key: key);




  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,

      children: <Widget>[
        Text(
          name_text,
          style: TextStyles.airbnbCerealBook.copyWith(
            color: Colors.grey
          )
          ,textAlign: TextAlign.center
          ,
        ),

        Text(
          value_text,
          style: TextStyles.airbnbCerealMedium.copyWith(
              color: Colors.black
          ),
          textAlign: TextAlign.center
          ,
        )
      ],

    );
  }

}