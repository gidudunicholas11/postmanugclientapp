import 'package:flutter/material.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/theme.dart';
typedef OnPressed();
class CustomButton extends StatelessWidget{
  final String title;
  final OnPressed onPressed;
  final Color backgroundColor;
  final Color textColor;
//  final TextStyle textStyle;

  const CustomButton({Key key, this.title, this.onPressed, this.backgroundColor, this.textColor}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: EdgeInsets.only(left: 10.0, right: 10),
      child: RaisedButton(
        textColor: (textColor!=null)?textColor:Colors.white,
        color: (backgroundColor!=null)?backgroundColor:MyTheme.milanoRed,
        child: Padding(padding: EdgeInsets.only(
          left: 20,
          right: 20
        ),
        child: Text(
          this.title,
          style: TextStyles.airbnbCerealBook.copyWith(
              color: (textColor!=null)?textColor:Colors.white
          ),
        ),

        ),
        onPressed: () {
          if(onPressed!=null){
            onPressed();
          }
        },
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(18.0),
            side: BorderSide(color: MyTheme.milanoRed)
        ),
      ),
    );
  }

}



class CustomButton2 extends StatelessWidget{
  final String title;
  final OnPressed onPressed;
  final Color backgroundColor;
  final Color textColor;
//  final TextStyle textStyle;

  const CustomButton2({Key key, this.title, this.onPressed, this.backgroundColor, this.textColor}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin:
      const EdgeInsets.symmetric(vertical: 16.0, horizontal: 30),
      child: ButtonTheme(
        height: 50,
        child: FlatButton(
          onPressed: () {
            if(onPressed!=null){
              onPressed();
            }

          },
          child: Center(
              child: Text(
                title.toUpperCase(),
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              )),
        ),
      ),
      decoration: BoxDecoration(
          color: (backgroundColor!=null)?backgroundColor:MyTheme.milanoRed,
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
                color: Colors.red.shade200,
                offset: Offset(1, -2),
                blurRadius: 5),
            BoxShadow(
                color: Colors.red.shade200,
                offset: Offset(-1, 2),
                blurRadius: 5)
          ]),
    );
  }

}













