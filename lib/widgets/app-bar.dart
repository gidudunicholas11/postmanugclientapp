import 'package:flutter/material.dart';
import 'package:postmanug/util/theme.dart';
typedef OnBackIconPressed();




class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {

  final String title_str;
  bool has_icon = false;
  Icon custom_icon = null;
  OnBackIconPressed onBackIconPressed = null;


  CustomAppBar({Key key, this.title_str, this.has_icon, this.custom_icon, this.onBackIconPressed}) : preferredSize = Size.fromHeight(kToolbarHeight), super(key: key);

  @override
  final Size preferredSize; // default is 56.0

  @override
  _CustomAppBarState createState() => _CustomAppBarState(this.title_str, this.has_icon, this.custom_icon, this.onBackIconPressed);
}

class _CustomAppBarState extends State<CustomAppBar>{
  final String title_str;
  bool has_icon = false;
  Icon custom_icon = null;
  OnBackIconPressed onBackIconPressed = null;

  _CustomAppBarState(this.title_str, this.has_icon, this.custom_icon, this.onBackIconPressed);


  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Center(
        child: Text(this.title_str),
      ),
     backgroundColor: MyTheme.primaryColor,
      leading: getLeadingIcon(),

      /*leading:
      */

    );
  }

  Widget getLeadingIcon() {
//    return null;

    if(this.has_icon){
     return IconButton(
       icon:getIcon()
       ,
       onPressed: (){

         if(onBackIconPressed==null){
           Navigator.pop(context, false);
         }else{
           onBackIconPressed();
         }

       },
     );
    }else{
      return Container();
    }

  }

  Icon getIcon() {
    if(custom_icon==null){
      return Icon(Icons.arrow_back);
    }else{
      return custom_icon;
    }
  }
}