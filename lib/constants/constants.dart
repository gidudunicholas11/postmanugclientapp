final String SIGN_IN = 'signin';
final String SIGN_UP = 'signup';
final String SPLASH_SCREEN = 'splashscreen';

//hope

class URLS {
  static final String host = "http://52.89.82.130:9015/";
//  static final String host = "http://54.186.202.6:9015/";

//  static final String host = "http://02261277.ngrok.io/";
  static final String api = host + "api/v2/";

  static final String login = api + "api-token-auth/";
  static final String registrations = api + "registrations/";
  static final String verify_code = api + "registrations/%s/verify_code/";
  static final String addresses = api + "addresses/";
  static final String clientprofile = api + "clientprofiles/";
  static final String zones = api + "zones/";
}
