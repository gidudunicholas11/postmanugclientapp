// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// FloorGenerator
// **************************************************************************

class $FloorAppDatabase {
  /// Creates a database builder for a persistent database.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder databaseBuilder(String name) =>
      _$AppDatabaseBuilder(name);

  /// Creates a database builder for an in memory database.
  /// Information stored in an in memory database disappears when the process is killed.
  /// Once a database is built, you should keep a reference to it and re-use it.
  static _$AppDatabaseBuilder inMemoryDatabaseBuilder() =>
      _$AppDatabaseBuilder(null);
}

class _$AppDatabaseBuilder {
  _$AppDatabaseBuilder(this.name);

  final String name;

  final List<Migration> _migrations = [];

  Callback _callback;

  /// Adds migrations to the builder.
  _$AppDatabaseBuilder addMigrations(List<Migration> migrations) {
    _migrations.addAll(migrations);
    return this;
  }

  /// Adds a database [Callback] to the builder.
  _$AppDatabaseBuilder addCallback(Callback callback) {
    _callback = callback;
    return this;
  }

  /// Creates the database and initializes it.
  Future<AppDatabase> build() async {
    final path = name != null
        ? join(await sqflite.getDatabasesPath(), name)
        : ':memory:';
    final database = _$AppDatabase();
    database.database = await database.open(
      path,
      _migrations,
      _callback,
    );
    return database;
  }
}

class _$AppDatabase extends AppDatabase {
  _$AppDatabase([StreamController<String> listener]) {
    changeListener = listener ?? StreamController<String>.broadcast();
  }

  LocationEntityDao _locationEntityDaoInstance;

  Future<sqflite.Database> open(String path, List<Migration> migrations,
      [Callback callback]) async {
    return sqflite.openDatabase(
      path,
      version: 1,
      onConfigure: (database) async {
        await database.execute('PRAGMA foreign_keys = ON');
      },
      onOpen: (database) async {
        await callback?.onOpen?.call(database);
      },
      onUpgrade: (database, startVersion, endVersion) async {
        await MigrationAdapter.runMigrations(
            database, startVersion, endVersion, migrations);

        await callback?.onUpgrade?.call(database, startVersion, endVersion);
      },
      onCreate: (database, version) async {
        await database.execute(
            'CREATE TABLE IF NOT EXISTS `LocationEntity` (`id` INTEGER, `latitude` REAL, `longitude` REAL, PRIMARY KEY (`id`))');

        await callback?.onCreate?.call(database, version);
      },
    );
  }

  @override
  LocationEntityDao get locationEntityDao {
    return _locationEntityDaoInstance ??=
        _$LocationEntityDao(database, changeListener);
  }
}

class _$LocationEntityDao extends LocationEntityDao {
  _$LocationEntityDao(this.database, this.changeListener)
      : _queryAdapter = QueryAdapter(database, changeListener),
        _locationEntityInsertionAdapter = InsertionAdapter(
            database,
            'LocationEntity',
            (LocationEntity item) => <String, dynamic>{
                  'id': item.id,
                  'latitude': item.latitude,
                  'longitude': item.longitude
                },
            changeListener),
        _locationEntityUpdateAdapter = UpdateAdapter(
            database,
            'LocationEntity',
            ['id'],
            (LocationEntity item) => <String, dynamic>{
                  'id': item.id,
                  'latitude': item.latitude,
                  'longitude': item.longitude
                },
            changeListener);

  final sqflite.DatabaseExecutor database;

  final StreamController<String> changeListener;

  final QueryAdapter _queryAdapter;

  static final _locationEntityMapper = (Map<String, dynamic> row) =>
      LocationEntity(row['id'] as int, row['latitude'] as double,
          row['longitude'] as double);

  final InsertionAdapter<LocationEntity> _locationEntityInsertionAdapter;

  final UpdateAdapter<LocationEntity> _locationEntityUpdateAdapter;

  @override
  Future<List<LocationEntity>> findAllLocations() async {
    return _queryAdapter.queryList('SELECT * FROM LocationEntity',
        mapper: _locationEntityMapper);
  }

  @override
  Stream<LocationEntity> findLocationById(int id) {
    return _queryAdapter.queryStream(
        'SELECT * FROM LocationEntity WHERE id = ?',
        arguments: <dynamic>[id],
        tableName: 'LocationEntity',
        mapper: _locationEntityMapper);
  }

  @override
  Future<void> insertLocation(LocationEntity locationEntity) async {
    await _locationEntityInsertionAdapter.insert(
        locationEntity, sqflite.ConflictAlgorithm.abort);
  }

  @override
  Future<void> updateLocation(LocationEntity locationEntity) async {
    await _locationEntityUpdateAdapter.update(
        locationEntity, sqflite.ConflictAlgorithm.abort);
  }
}
