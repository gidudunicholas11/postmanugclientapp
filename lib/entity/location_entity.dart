import 'package:floor/floor.dart';

@entity
class LocationEntity {
  @primaryKey
  final int id;
  double latitude;
  double longitude;
  LocationEntity(this.id, this.latitude,this.longitude);
}