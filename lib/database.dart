import 'dart:async';
import 'package:floor/floor.dart';
import 'package:path/path.dart';
import 'package:postmanug/dao/location_entity_dao.dart';
import 'package:postmanug/entity/location_entity.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

//flutter packages pub run build_runner watch

part 'database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [LocationEntity])
abstract class AppDatabase extends FloorDatabase {
  LocationEntityDao get locationEntityDao;
}