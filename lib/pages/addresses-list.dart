//
import 'dart:convert';

import 'package:dropdown_menu/dropdown_menu.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
//import 'package:image_selector_formfield/image_selector_formfield.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/pages/add-address.dart';
import 'package:postmanug/pages/edit-address.dart';
import 'package:postmanug/util/CustomShapeClipper.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/widgets/app-bar.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/date-time-pickers.dart';
import 'package:postmanug/widgets/inline-double-display.dart';
import 'package:postmanug/widgets/text-input-field.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';


class AddressesList extends StatefulWidget{

  const AddressesList({Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AddressesListState();
  }

}


enum AccountType { individual, business }

class AddressesListState extends State<AddressesList>{


  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  List items = new List();


  void _onRefresh() async{

    String res = await HttpUtil.get(
        URLS.clientprofile+(await Util.getPrefString(Util.CLIENT_ID) as String)+"/addresses/",with_auth: true
    );

    print(URLS.clientprofile+(await Util.getPrefString(Util.CLIENT_ID) as String)+"/addresses/");

    print(res);



    try {

      List<dynamic> list = json.decode(res);

      print(list.length);



      items = json.decode(res);
      print(items.length);
      setState(() {

      });

      Util.saveListPrefString(Util.MY_ADDRESSES, res);

    } on FormatException catch (e) {

    }

    // monitor network fetch
//    await Future.delayed(Duration(milliseconds: 1000));
//    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
//    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
//    items.add((items.length+1).toString());
//    if(mounted)
//      setState(() {
//
//      });
//    _refreshController.loadComplete();
  }

  @override
  Future<void> initState()  {

    super.initState();

    getListItems();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: CustomAppBar(
        title_str: "Addresses",
        has_icon: true,
        custom_icon: null,
        onBackIconPressed: null,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,

          child:

          SmartRefresher(
            enablePullDown: true,
            enablePullUp: true,
            header: WaterDropHeader(),
            footer: CustomFooter(
              builder: (BuildContext context,LoadStatus mode){
                Widget body ;
                if(mode==LoadStatus.idle){
                  body =  Text("pull up load");
                }
                else if(mode==LoadStatus.loading){
                  body =  CupertinoActivityIndicator();
                }
                else if(mode == LoadStatus.failed){
                  body = Text("Load Failed!Click retry!");
                }
                else if(mode == LoadStatus.canLoading){
                  body = Text("release to load more");
                }
                else{
                  body = Text("No more Data");
                }
                return Container(
                  height: 55.0,
                  child: Center(child:body),
                );
              },
            ),
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            child: ListView.separated(
              separatorBuilder: (context, index) => Divider(
                color: Colors.transparent,
              ),
              itemCount: items.length,

              itemBuilder: (context, index) {
                return MailDetailsWidget(item:items[index]);
              },


            )
          )





        ),
      ),

        floatingActionButton:

        new FloatingActionButton(
            elevation: 0.0,
            child: InkWell(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>
                      AddAddress(
                      )
                  ),
                );
              },
              child:
              Icon(
                Icons.add,
                color: Colors.white,

              ),
            ),
            backgroundColor: new Color(0xFFE57373),
            onPressed: (){}
        )
    );
  }

  Future<void> getListItems() async {
    List l = await Util.getPrefList(Util.MY_ADDRESSES);
    if(l!=null){
      items=l;
      setState(() {

      });
    }else{
      _onRefresh();
    }
  }

}

class MailDetailsWidget extends StatelessWidget {
  var item;

  var _count = 0;
  var _tapPosition;

  MailDetailsWidget({Key key, this.item}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();
    // TODO: implement build
    return InkWell(
      onTapDown: _storePosition,
      onLongPress: (){

        showMenu(
          position: RelativeRect.fromRect(
              _tapPosition & Size(40, 40), // smaller rect, the touch area
              Offset.zero & overlay.size   // Bigger rect, the entire screen
          ),
          items: <PopupMenuEntry>[
        PopupMenuItem(
        value: 1,
          child: InkWell(
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>
                    EditAddress(
                      item: item,
                    )
                ),
              );
            },
            child: Row(
              children: <Widget>[
                Icon(Icons.edit_location),
                Text("Edit Address"),
              ],
            ),
          ),

        )
        ],
        context: context,
        );
      },
      child: Padding(
        padding: EdgeInsets.all(5),
        child:Container(
          padding: EdgeInsets.all(16),


          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            color: MyTheme.light_grey,
          ),


          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Map Code: "+item['map_code'],
                style: TextStyles.airbnbCerealMedium.copyWith(
                    color: Colors.black,
                    fontSize: MyTheme.heading4
                ),
              ),
              SizedBox(
                height: 5,
              ),

              Text(
                "house number: "+item['house_number'],
                style: TextStyles.airbnbCerealBook.copyWith(
                    color: MyTheme.shipGray,
                    fontSize: MyTheme.body4
                ),
              ),

              Text(
                "Street: "+item['street'],
                style: TextStyles.airbnbCerealBook.copyWith(
                    color: MyTheme.shipGray,
                    fontSize: MyTheme.body4
                ),
              ),

              Text(
                "Physical Address: "+item['physical_address'],
                style: TextStyles.airbnbCerealBook.copyWith(
                    color: MyTheme.shipGray,
                    fontSize: MyTheme.body4
                ),
              ),

              /* Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Fee: 320,000 Ugx",
                    style: TextStyles.airbnbCerealBook.copyWith(
                        color: MyTheme.shipGray,
                        fontSize: MyTheme.body4
                    ),
                  ),

                  Text(
                    "22nd Jan 2020",
                    style: TextStyles.airbnbCerealBook.copyWith(
                        color: MyTheme.shipGray,
                        fontSize: MyTheme.body4
                    ),
                  )
                ],
              )*/
            ],
          ),
        ),
      ),
    );
  }

  void _storePosition(TapDownDetails details) {
    _tapPosition = details.globalPosition;
  }

}
