//
import 'package:dropdown_menu/dropdown_menu.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
//import 'package:image_selector_formfield/image_selector_formfield.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:postmanug/util/CustomShapeClipper.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/widgets/app-bar.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/date-time-pickers.dart';
import 'package:postmanug/widgets/inline-double-display.dart';
import 'package:postmanug/widgets/text-input-field.dart';


class MailHistory extends StatefulWidget{

  const MailHistory({Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MailHistoryState();
  }

}


enum AccountType { individual, business }

class MailHistoryState extends State<MailHistory>{




  @override
  Future<void> initState()  {



    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: CustomAppBar(
        title_str: "Mail History",
        has_icon: true,
        custom_icon: null,
        onBackIconPressed: null,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,

          child: ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.transparent,
            ),
            itemCount: 30,
            itemBuilder: (context, index) {
              return MailDetailsWidget();
            },


          )

        ),
      ),
    );
  }
}

class MailDetailsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(
      padding: EdgeInsets.all(5),
        child:Container(
          padding: EdgeInsets.all(16),


          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            color: MyTheme.light_grey,
          ),


          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Dispatch",
                style: TextStyles.airbnbCerealMedium.copyWith(
                    color: Colors.black,
                    fontSize: MyTheme.heading4
                ),
              ),
              SizedBox(
                height: 5,
              ),

              Text(
                "Package Delivered to 897363 CT.56",
                style: TextStyles.airbnbCerealBook.copyWith(
                    color: MyTheme.shipGray,
                    fontSize: MyTheme.body4
                ),
              ),

              Text(
                "By: Postman NO. 2342",
                style: TextStyles.airbnbCerealBook.copyWith(
                    color: MyTheme.shipGray,
                    fontSize: MyTheme.body4
                ),
              )
              ,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Fee: 320,000 Ugx",
                    style: TextStyles.airbnbCerealBook.copyWith(
                        color: MyTheme.shipGray,
                        fontSize: MyTheme.body4
                    ),
                  ),

                  Text(
                    "22nd Jan 2020",
                    style: TextStyles.airbnbCerealBook.copyWith(
                        color: MyTheme.shipGray,
                        fontSize: MyTheme.body4
                    ),
                  )
                ],
              )
            ],
          ),
        ),
    );
  }
}
