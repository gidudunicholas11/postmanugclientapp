

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:http/http.dart' as http;
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/indicator/ball_spin_fade_loader_indicator.dart';
import 'package:loading/loading.dart';
import 'package:location/location.dart';
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/entity/location_entity.dart';
import 'package:postmanug/pages/add-address.dart';
import 'package:postmanug/pages/addresses-list.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/util/validator.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/text-input-field.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'package:enhanced_drop_down/enhanced_drop_down.dart';


import '../database.dart';
import 'account-created.dart';
import 'add-first-address.dart';
import 'home.dart';
// import 'package:social_login/social_login.dart';

class GenerateAddressCodeNew extends StatefulWidget{

  var item = {
  'latitude':0,
  'longitude':0,
  'post_office_box	':"32",
  'plot_number	':"12",
  'physical_address':"Wandegeya",
  'national_post_code':"1200",
  'street':"Bombo Road",
  'account_type':1,

  };

  GenerateAddressCodeNew({Key key, this.item}) : super(key: key);




  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new GenerateAddressCodeNewState();
  }

}

class GenerateAddressCodeNewState extends State<GenerateAddressCodeNew> {


  final _formKey = GlobalKey<FormState>();

  Location _locationService  = new Location();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

//    showDialog1();

 /* widget.item = {
    'latitude':0,
    'longitude':0,
    'post_office_box	':"32",
    'plot_number	':"12",
    'physical_address':"Wandegeya",
    'national_post_code':"1200",
    'street':"Bombo Road",
    'account_type':1,

  };*/
    _registerListener();

  }



  @override
  void dispose() {
    super.dispose();

    _removeListener();
  }

  static void backgroundCallback(List<LocationData> locations) async {
//    print('Sample app received data location: $locations');

    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings);

    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'location_update', 'Location Updates', 'You will receive location updates here',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//    await flutterLocalNotificationsPlugin.show(
//        0, 'New Location Received !', "${locations[0].latitude}, ${locations[0].longitude}", platformChannelSpecifics);

    final database = await $FloorAppDatabase.databaseBuilder(Util.DB_NAME).build();

    var locs = await database.locationEntityDao.findAllLocations();
//    print(locs.length);
    if(locs.length>0){
      LocationEntity loc = locs.elementAt(0);
      loc.latitude=locations[0].latitude;
      loc.longitude=locations[0].longitude;


    }else{
      LocationEntity loc = new LocationEntity(1, locations[0].latitude,locations[0].longitude);
      await database.locationEntityDao.insertLocation(loc);
    }





  }

  void _registerListener() async {
    bool _permission = await _locationService.requestPermission();
    print("Permission: $_permission");
    if (_permission) {
      bool statusBackgroundLocation = await _locationService.registerBackgroundLocation(backgroundCallback);
      print("statusBackgroundLocation: $statusBackgroundLocation");
    } else {
      print("Permission denied");
    }

  }

  void _removeListener() async {
    await _locationService.removeBackgroundLocation();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Size size = MediaQuery
        .of(context)
        .size;


    return Scaffold(
//      backgroundColor: MyTheme.primaryColor,

      appBar: new AppBar(
        title: new Text("Generate Address Code"),
        centerTitle: false,
        elevation: 0.0,
        backgroundColor: MyTheme.primaryColor,


      ),


      body: SingleChildScrollView(
          child: Padding(padding: EdgeInsets.all(16),

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                
                Center(
                  child:Text("Generate Address code",
                  style: TextStyles.airbnbCerealMedium.copyWith(
                    fontSize: 18,
                    color: Colors.black
                  ),
                  ),
                ),

                Center(
                  child:Text("Select start to generate your address code.",
                    style: TextStyles.airbnbCerealBook.copyWith(
                        fontSize: 16,
                        color: Colors.black
                    ),
                  ),
                ),
                SizedBox(height: 30,),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new OutlineButton(
                        child: new Text("DO LATER"),
                        onPressed: (){

                        },
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0))
                    ),

                    SizedBox(width: 50,),

                    new OutlineButton(
                        child: Text("START",
                          style: TextStyles.airbnbCerealMedium.copyWith(
                              fontSize: 18,
                              color: Colors.black
                          ),
                        ),
                        onPressed: (){
                          /*Navigator.pop(context);
                          Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AddFirstAddress(
                          )));*/

                          showDialog1();


                        },
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0))
                    )
                  ],
                )








              ],
            )
            ,)
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Future<void> showDialog1() async {

    final database = await $FloorAppDatabase.databaseBuilder(Util.DB_NAME).build();

    var locs = await database.locationEntityDao.findAllLocations();
//    print(locs.length);
    if(locs.length>0){
      LocationEntity loc = locs.elementAt(0);



      Alert(
          closeFunction: onClose(),
          context: context,
          title: "Generating address code",
          content: Column(
            children: <Widget>[
              Center(
                child: Loading(indicator: BallSpinFadeLoaderIndicator(), size: 100.0,color: Colors.pink),
              ),

              Text("Please wait...",
                style: TextStyles.airbnbCerealBook.copyWith(
                    fontSize: 16,
                    color: Colors.grey
                ),
              )

            ],
          ),
          buttons: [

          ]).show();


//    print("abc");
      String res = await HttpUtil.post(
          URLS.clientprofile+(await Util.getPrefString(Util.CLIENT_ID) as String)+"/addresses/",{
        "coordinates": loc.latitude.toString()+","+loc.longitude.toString(),
    'national_post_code':widget.item['national_post_code'].toString(),
    'street':widget.item['street'].toString(),
    'house_number':widget.item['plot_number'].toString(),
    'physical_address':widget.item['physical_address'].toString(),
      }, with_auth: true
      );
/* String res = await HttpUtil.patch(
          "http://54.186.202.6:9015/api/v2/clientprofiles/7713088906464858601/addresses/9717849364024857066/",{
      "coordinates": loc.latitude.toString()+","+loc.longitude.toString(),
    'national_post_code':widget.item['national_post_code'].toString(),
    'street':widget.item['street'].toString(),
    'house_number':widget.item['plot_number'].toString(),
    'physical_address':widget.item['physical_address'].toString(),
      }, with_auth: true
      );*/







    var rp = json.decode(res);

      if(rp.containsKey('url')) {

        Navigator.pop(context);


        Util.saveAddItemToListPrefString(Util.MY_ADDRESSES, res);
        

        Alert(
          closeFunction: onClose(),
          context: context,
          title: "Generate address code",
          content: Column(
            children: <Widget>[
              Text(rp['map_code'],
                style: TextStyles.airbnbCerealBook.copyWith(
                    fontSize: 25,
                    color: MyTheme.primaryColor
                ),
              ),

              Text("This is the code you shall use when \nsending and receiving packages. \nPlease memorize it",
                style: TextStyles.airbnbCerealBook.copyWith(
                    fontSize: 16,
                    color: Colors.grey
                ),
              )

            ],
          ),
          buttons: [
            DialogButton(
              child: Text(
                "Okay",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () async {
                Navigator.pop(context);
                Navigator.pop(context);
                var user = json.decode(await Util.getPrefString(Util.LAST_LOGIN_USER));
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) =>
                        ProfilePreviewScreen(
                          item: user,
                          address: rp,

                        )
                    )
                );
              },
              width: 120,
            )
          ],).show();
      }else{
        Alert(
          context: context,
          type: AlertType.error,
          title: "Notification",
          desc: "Error adding an address",
          /*buttons: [
                        DialogButton(
                          child: Text(
                            "Okay",
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) =>
                                    HomePage(

                                    )
                                )
                            );
                          },
                          width: 120,
                        )
                      ],*/
        ).show();
      }


    }else{
      Alert(
        context: context,
        type: AlertType.error,
        title: "Notification",
        desc: "Error getting location, try again",
        /*buttons: [
                        DialogButton(
                          child: Text(
                            "Okay",
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) =>
                                    HomePage(

                                    )
                                )
                            );
                          },
                          width: 120,
                        )
                      ],*/
      ).show();
      _removeListener();
      _registerListener();
    }






  }



}

onClose() {
}



