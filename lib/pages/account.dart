import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_menu/dropdown_menu.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
//import 'package:image_selector_formfield/image_selector_formfield.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:postmanug/pages/EditClient.dart';
import 'package:postmanug/util/CustomShapeClipper.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/widgets/app-bar.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/date-time-pickers.dart';
import 'package:postmanug/widgets/inline-double-display.dart';
import 'package:postmanug/widgets/text-input-field.dart';

class ProfileScreen extends StatefulWidget {
  var item;
  var address = null;

  ProfileScreen({Key key, this.item, this.address}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProfileScreenState();
  }
}

enum AccountType { individual, business }

class ProfileScreenState extends State<ProfileScreen> {
  Map user_item = {
    'profile': {
      'user': {'username': "", 'first_name': "", 'last_name': "", 'email': ""},
      'date_of_birth': "",
      'gender': "",
      'phone_number': "",
      'photo': 'http://54.186.202.6:9015/media/photos/clients/no-img.jpg',
      'client_uuid': '',
      'created_at': '',
      'post_office_box': '',
      'physical_address': '',
      'device_id': '',
      'client_id': '',
      'default_pin_activated': '',
      'account_type': ''
    },
    'token': ''
  };

  AccountType _account_type = AccountType.individual;

  @override
  Future<void> initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title_str: "Profile",
        has_icon: true,
        custom_icon: null,
        onBackIconPressed: null,
      ),
      body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Stack(
            children: <Widget>[
              ClipPath(
                  clipper: CustomShapeClipper(),
                  child: Container(
                    height: 200,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: MyTheme.milanoRed,
                    ),
                  )),
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    new Center(
                        child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 160.0,
                          height: 160.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: AdvancedNetworkImage(
                                widget.item['profile']['photo'],
//                            header: header,
                                useDiskCache: true,
                                cacheRule:
                                    CacheRule(maxAge: const Duration(days: 7)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )),
                    SizedBox(
                      height: 20,
                    ),
                    InlineDoubleDisplay(
                      name_text: "Phone",
                      value_text: widget.item['profile']['user']['username'],
                    ),
                    Divider(
                      color: Colors.grey,
                      endIndent: 30,
                      indent: 30,
                    ),
                    InlineDoubleDisplay(
                      name_text: "Name",
                      value_text: widget.item['profile']['user']['first_name'] +
                          " " +
                          widget.item['profile']['user']['last_name'],
                    ),
                    Divider(
                      color: Colors.grey,
                      endIndent: 30,
                      indent: 30,
                    ),
                    InlineDoubleDisplay(
                      name_text: "District",
                      value_text: "Kampala",
                    ),
                    Divider(
                      color: Colors.grey,
                      endIndent: 30,
                      indent: 30,
                    ),
                    InlineDoubleDisplay(
                      name_text: "National Postcode",
                      value_text: (widget.address == null)
                          ? ""
                          : widget.address['national_post_code'],
                    ),
                    Divider(
                      color: Colors.grey,
                      endIndent: 30,
                      indent: 30,
                    ),
                    InlineDoubleDisplay(
                      name_text: "Adress Code",
                      value_text: (widget.address == null)
                          ? ""
                          : widget.address['map_code'],
                    ),
                    Divider(
                      color: Colors.grey,
                      endIndent: 30,
                      indent: 30,
                    ),
                    InlineDoubleDisplay(
                      name_text: "Country",
                      value_text: (widget.address == null)
                          ? ""
                          : widget.address['country'],
                    ),
                    Divider(
                      color: Colors.grey,
                      endIndent: 30,
                      indent: 30,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomButton(
                      title: "EDIT",
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MyEdit(
                              phone: widget.item['profile']['user']['username'],
                              fname: widget.item['profile']['user']
                                  ['first_name'],
                              lname: widget.item['profile']['user']
                                  ['last_name'],
                              district: "Kampala",
                              postcode: (widget.address == null)
                                  ? ""
                                  : widget.address['national_post_code'],
                              addresscode: (widget.address == null)
                                  ? ""
                                  : widget.address['map_code'],
                              country: (widget.address == null)
                                  ? ""
                                  : widget.address['country'],
                            ),

                            // phoneNumber: "+256706080109",
                          ),
                        );
                      },
                    )
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
