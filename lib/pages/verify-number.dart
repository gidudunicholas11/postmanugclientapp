import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:postmanug/components/exp_info_tile.dart';
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/widgets/app-bar.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:recase/recase.dart';
import 'package:url_launcher/url_launcher.dart';

import 'input-verification-code.dart';


class RequestVerification extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RequestVerificationState();
  }

}


enum AccountType { individual, business }

class RequestVerificationState extends State<RequestVerification>{
  AccountType _account_type = AccountType.individual;


  final _formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  int _radioValue1 = 1;

  String phoneNumber = '';
  bool valid = false;
  bool is_sending = false;

  List addresses = List();



  @override
  void initState() {
    super.initState();


  }

  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;

    return MaterialApp(
      debugShowCheckedModeBanner: false,

//      theme: MyTheme.theme,
      home: new Scaffold(
        backgroundColor: const Color(0xFFF1F1EF),
          appBar: CustomAppBar(
            title_str: "Verify Phone Number",
            has_icon: true,
            custom_icon: null,
            onBackIconPressed: null,
          ),

        body: SingleChildScrollView(
          child: Column(

          children: <Widget>[

            Center(

              child:Padding(padding: EdgeInsets.all(16),
                child: Image.asset(
                  "assets/images/app_icon.png",

                  width: 80,
                ),),
            ),


            Padding(padding: EdgeInsets.all(20),
              child: Text(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna",
                style: TextStyles.airbnbCerealBook.copyWith(
                  color: Colors.black,

                ),
                textAlign: TextAlign.center,
              ),),

            Padding(padding: EdgeInsets.all(20),
              child: Text(
                "Lorem ipsum dolor",
                style: TextStyles.airbnbCerealMedium.copyWith(
                    color: Colors.black,
                    fontSize: MyTheme.heading2

                ),
                textAlign: TextAlign.center,
              ),),

            Center(
                child:Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,

                  children: <Widget>[


                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Radio(
                          value: 1,
                          groupValue: _radioValue1,
                          onChanged: _handleRadioValueChange1,
                        ),
                        new Text(
                          'Individual',
                          style: new TextStyle(fontSize: 16.0),
                        ),
                      ],
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Radio(
                          value: 2,
                          groupValue: _radioValue1,
                          onChanged: _handleRadioValueChange1,
                        ),
                        new Text(
                          'Business',
                          style: new TextStyle(fontSize: 16.0),
                        ),
                      ],
                    )



                    /*RadioListTile<AccountType>(
                            title: const Text('Individual'),
                            value: AccountType.individual,
                            groupValue: _account_type,
                            onChanged: (AccountType value) {
                              setState(() {
                                _account_type = value;
                              });
                            },
                          ),
                          RadioListTile<AccountType>(
                            title: const Text('Business'),
                            value: AccountType.business,
                            groupValue: _account_type,
                            onChanged: (AccountType value) {
                              setState(() {
                                _account_type = value;
                              });
                            },
                          ),*/
                  ],
                )
            ),

            Padding(padding: EdgeInsets.all(20),
              child: Text(
                "Phone Number",
                style: TextStyles.airbnbCerealMedium.copyWith(
                    color: Colors.black,
                    fontSize: MyTheme.heading2

                ),
                textAlign: TextAlign.center,
              ),),


            Padding(padding: EdgeInsets.only(
                left: 50, right: 50,
                top: 10, bottom: 10
            ),
              child:
              InternationalPhoneNumberInput.withCustomDecoration(
                  onInputChanged: onPhoneNumberChanged,
                  onInputValidated: onInputChanged,
                  countries:[
                    "UG"
                  ],
//                  textFieldController: ,
                  initialCountry2LetterCode: 'UG',
                  inputDecoration: InputDecoration(
                    hintText: 'Enter phone number',
                    errorText: valid ? null : 'Invalid',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                    ),
                  )
              )
              ,

            ),


            CustomButton(
              title: "NEXT",
              backgroundColor: is_sending?Colors.grey:MyTheme.primaryColor,
              onPressed: ()async{
                if(!is_sending) {
                  if (valid) {
                    setState(() {
                      is_sending = true;
                    });
                    String res = await HttpUtil.post(
                        URLS.registrations, {
                      'phone_number': phoneNumber.substring(1),

                    }
                    );

                    setState(() {
                      is_sending = false;
                    });

                    print(res);

                    try {
                      var x = json.decode(res) as Map<String, dynamic>;


                      if (x.containsKey("reg_id")) {
                        Util.savePrefString(Util.ACCOUNT_TYPE_PREF, '$_radioValue1');
                        var data = json.decode(res);

                        var phone_number = x.keys.firstWhere(
                                (k) => x[k] == 'phone_number',
                            orElse: () => null);
                        Util.savePrefString(
                            Util.ACCOUNT_PHONE_PREF, data['phone_number']);
                        Util.savePrefString(Util.REG_ID, data['reg_id']);


//                     Util.savePrefString(Util.LAST_LOGIN_USER, res);
                        Navigator.pop(context);
                        Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) =>
                                VerificationCodeScreen(
                                  phoneNumber: phoneNumber,
                                )));
                      } else {
                        Util.basicAlert(
                            context, "Error while getting code, try again");
                      }
                    } on FormatException catch (e) {
                      Util.snack(
                          context, "Error while getting code, try again");
                    }
                  }
                }





              },
            )





          ],
        )
        ),










      ),
    );
  }

  Future<void> getAddresses() async {
    String res = await HttpUtil.get(
        URLS.addresses,with_auth: true
    );

    //addresses= json.decode(res);
    //_searchDelegate = _SearchAppBarDelegate(addresses,hist);
    print(res);

    setState(() {

    });
  }

  void _handleRadioValueChange1(int value) {

    setState(() {
      _radioValue1 = value;


    });

  }


  void onPhoneNumberChanged(PhoneNumber phoneNumber) {
    print(phoneNumber);
    setState(() {
      this.phoneNumber = phoneNumber.toString();
    });
  }

  void onInputChanged(bool value) {
    print(value);
    setState(() {
      valid = value;
    });
  }


}

