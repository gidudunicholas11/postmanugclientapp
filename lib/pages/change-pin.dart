

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:http/http.dart' as http;
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/pages/Login.dart';
import 'package:postmanug/pages/addresses-list.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/util/validator.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/text-input-field.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'package:enhanced_drop_down/enhanced_drop_down.dart';


import 'home.dart';
// import 'package:social_login/social_login.dart';


class ChangePin extends StatefulWidget{





  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new ChangePinState();
  }

}

class ChangePinState extends State<ChangePin> {
  TextEditingController controller_old_pin;
  TextEditingController controller_new_pin;
  TextEditingController controller_conf_new_pin;

  final _formKey = GlobalKey<FormState>();





  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    controller_old_pin = new TextEditingController();
    controller_new_pin = new TextEditingController();
    controller_conf_new_pin = new TextEditingController();

  }


  final _scaffoldKeys = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Size size = MediaQuery
        .of(context)
        .size;


    return Scaffold(
      key: _scaffoldKeys,
//      backgroundColor: MyTheme.primaryColor,

      appBar: new AppBar(
        title: new Text("Change Pin"),
        centerTitle: false,
        elevation: 0.0,
        backgroundColor: MyTheme.primaryColor,


      ),


      body: SingleChildScrollView(
          child: Padding(padding: EdgeInsets.all(16),

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[

                /* SizedBox(
              height: 100,
            ),

            Center(
              child:
              Image.asset(
                "images/logo.png",
                height: 80,
                width: 80,
              ),
            ),*/

                /*SizedBox(
              height: 80,
            ),*/




                Form(
                    key: _formKey,
                    child: Column(
                        children: <Widget>[


                          MyTextInputField(
                            hint: "Old Pin",
                            show_label: true,
                            Label: "Old Pin",
                            controller: controller_old_pin,

                            validator: (value) {
                              return Validator().validateRequired(value);
                            },

                          ),

                          MyTextInputField(
                            hint: "New Pin",
                            show_label: true,
                            Label: "New Pin",
                            controller: controller_new_pin,
                            validator: (value) {
                              return Validator().validateName(value);
                            },

                          ),

                          MyTextInputField(
                            hint: "Confirm New Pin",
                            show_label: true,
                            Label: "Confirm New Pin",
                            controller: controller_conf_new_pin,
                            validator: (value) {
                              if(value != controller_new_pin.text){
                                return 'Passwords do not match';
                              }else{
                                return Validator().validateName(value);
                              }
                            },

                          ),




                        ]
                    )
                ),

                SizedBox(
                  height: 10,
                ),
                // A


                CustomButton2(
                  title: "Change Pin",
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      _scaffoldKeys.currentState.showSnackBar(
                          new SnackBar(duration: new Duration(seconds: 4), content:
                          new Row(
                            children: <Widget>[
                              new CircularProgressIndicator(),
                              new Text("    changing pin ...")
                            ],
                          ),
                          ));

                      String res = await HttpUtil.post(
                          URLS.clientprofile+(await Util.getPrefString(Util.CLIENT_ID) as String)+"/change_password/",{
                        'password': controller_old_pin.text,
                        'new_password1': controller_new_pin.text,
                        'new_password2': controller_conf_new_pin.text,

                      }, with_auth: true
                      );

                      var e = jsonDecode(res);

                      if(res.contains('status')){
                        _scaffoldKeys.currentState.showSnackBar(
                            new SnackBar(
                              backgroundColor: MyTheme.milanoRed,
                              duration: new Duration(seconds: 5), content:
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Text(    e['status'].toString()),

                              ],
                            ),
                            ));

                        if(res.contains('Password changed')){
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) =>
                                Login()
                            ),
                          );
                        }

                      }else{
                        _scaffoldKeys.currentState.showSnackBar(
                            new SnackBar(
                              backgroundColor: MyTheme.milanoRed,
                              duration: new Duration(seconds: 4), content:
                            new Row(
                              children: <Widget>[
                                new CircularProgressIndicator(),
                                new Text('   Check your connection')
                              ],
                            ),
                            ));
                      }
                      print (res);

                    }else{
                    }
                  },
                ),
                SizedBox(
                  height: 10,
                ),


              ],
            )
            ,)
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }


}



