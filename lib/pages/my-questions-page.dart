import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rounded_letter/rounded_letter.dart';
import 'package:rounded_letter/shape_type.dart';

import 'my-questions-detail-page.dart';

class MyQuestionsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new MyQuestionsScreenState() ;
  }
}

class MyQuestionsScreenState extends State<MyQuestionsScreen> with AutomaticKeepAliveClientMixin {
  RefreshController _refreshController =
  RefreshController(initialRefresh: true);

  List items = ["","","",""];

  void _onRefresh() async{


    /*String token =await Util.getUserToken();


    String res = await HttpUtil.post(
        URLS.getmyquery,{
          'userid':token,
          'offset':0
    },with_auth: false
    );*/


//    print (res);






    /*try {

      var a = json.decode(res);

      items = a['results'];
      setState(() {

      });

    } on FormatException catch (e) {

    }*/

    // monitor network fetch
//    await Future.delayed(Duration(milliseconds: 1000));
//    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();



  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    /*items.add((items.length+1).toString());
    if(mounted)
      setState(() {

      });*/
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

        appBar: AppBar(
          backgroundColor: MyTheme.primaryColor,
          leading: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
            child:
            Icon(
              Icons.arrow_back,
              color: Colors.white,

            ),
          ),

          title: Text(
              "Help and Feed Back"
          ),
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(150),
            child: Theme(
              data: Theme.of(context).copyWith(accentColor: Colors.white),
              child: Container(
                height: 150,
                alignment: Alignment.center,
                child: ChatBoxWidget(),
              ),
            ),
          ),


        ),
      body:
        SmartRefresher(
          enablePullDown: true,
          enablePullUp: true,
          header: WaterDropHeader(),
          footer: CustomFooter(
            builder: (BuildContext context,LoadStatus mode){
              Widget body ;
              if(mode==LoadStatus.idle){
                body =  Text("pull up load");
              }
              else if(mode==LoadStatus.loading){
                body =  CupertinoActivityIndicator();
              }
              else if(mode == LoadStatus.failed){
                body = Text("Load Failed!Click retry!");
              }
              else if(mode == LoadStatus.canLoading){
                body = Text("release to load more");
              }
              else{
                body = Text("No more Data");
              }
              return Container(
                height: 55.0,
                child: Center(child:body),
              );
            },
          ),
          controller: _refreshController,
          onRefresh: _onRefresh,
          onLoading: _onLoading,
          child: ListView.builder(
            itemBuilder: (c, i) {

              var blog = items.elementAt(i);

              return /*InkWell(
              onTap: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>
                      BlogDetails(
                        blog_id: blog['id'],
                      )
                  ),
                );
              },
              child: ,
            )*/

                BlogItem(blog:blog);
            }



            ,

//          itemExtent: 200.0,
            itemCount: items.length,
          ),
        )
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

class BlogItem extends StatefulWidget {
  var blog;

  BlogItem({Key key, this.blog}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new BlogItemState();
  }


}

class BlogItemState extends State<BlogItem>{
  bool loading=false;
  var likes = 0;
  var comments = 0;
  var liked = 'no';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

//    print("sjshs "+widget.blog.toString());

    likes = 10;
    liked = "yes";
    comments = 20;

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
      onTap: (){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>
              MyQuestionsDetailScreen(
                id: int.parse("1"),
                item: widget.blog,
//                bl:widget.blog.toString()
              )
          ),
        );
      },
      child: Card(


        child:
        Padding(padding: EdgeInsets.all(5),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[

              RoundedLetter(
                text: "Q",
                shapeColor: MyTheme.primaryColor,
                shapeType: ShapeType.circle,
                borderColor: MyTheme.colorAccent,
                borderWidth: 2,
              ),

              SizedBox(width: 15,),

              Flexible(child:  Column(crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "some question",
                    style: TextStyles.airbnbCerealMedium.copyWith(
                      color: Colors.black,
                      fontSize: 16,

                    ),

                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,

                  ),

                  Text(
                    "the latest response",
                    style: TextStyles.airbnbCerealBook.copyWith(
                      color: Colors.grey,
                      fontSize: 15,

                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,

                  ),



                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      InkWell(
                        onTap: () async {
                          if(!loading){

                            if(liked=='no') {
                             /* String res = await HttpUtil.post(URLS.dolike, {
                                'type': 'query',
                                'uid': await Util.getUserToken() as String,
                                'id': widget.blog['id']
                              });*/


//                              print(res);

                             /* try{
                                int a = int.parse(res);

                                setState(() {
                                  likes=a;
                                  liked = 'yes';
                                });
                              }on FormatException catch (e) {
                              }*/
                            }else{
                              /*String res = await HttpUtil.post(URLS.dounlike, {
                                'type': 'query',
                                'uid': await Util.getUserToken() as String,
                                'id': widget.blog['id']
                              });
                              try{
                                int a = int.parse(res);

                                setState(() {
                                  likes=a;
                                  liked = 'no';
                                });
                              }on FormatException catch (e) {
                              }

                              print(res);*/
                            }
                          }else{

                          }

                        },
                        child: Icon(  ((liked=='no')?Icons.favorite_border:Icons.favorite),
                          size: 24,
                          color: ((liked=='no')?Colors.black:Colors.red),
                        ),
                      ),

                      Text(likes.toString(),
                        style: TextStyles.airbnbCerealBook.copyWith(
                            fontSize: 14,
                            color: Colors.grey
                        ) ,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.right,
                      ),

                      SizedBox(
                        width: 10,
                      ),

                      Text( comments.toString()+' comments',
                        style: TextStyles.airbnbCerealBook.copyWith(
                            fontSize: 14,
                            color: Colors.grey
                        ) ,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.right,
                      ),

                      SizedBox(
                        width: 10,
                      ),

                      Text("24-12-2019 9:30pm",
                        style: TextStyles.airbnbCerealBook.copyWith(
                            fontSize: 14,
                            color: Colors.grey
                        ) ,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.right,
                      ),

                    ],
                  )
                ],
              ))
            ],
          ),
        ),

      ),
    ) ;
  }

}

class ChatBoxWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new ChatBoxWidgetState();
  }

}

class ChatBoxWidgetState extends State<ChatBoxWidget> {

  TextEditingController _textController = new TextEditingController();
  var sending = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Size deviceSize = MediaQuery.of(context).size;
    return Container(
        color: Colors.white,
//              height: 120,
        alignment: Alignment.topCenter,
        child: new Column(
          children: <Widget>[
            new Container(
              padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
              child: new Row(
                children: <Widget>[
                  new Flexible(
                      child:Container(
                        color: MyTheme.seaShell,
                        padding: new EdgeInsets.all(7.0),

                        child: new ConstrainedBox(
                          constraints: new BoxConstraints(
                            minWidth: deviceSize.width,
                            maxWidth: deviceSize.width,
                            minHeight:  25.0,
                            maxHeight: 55.0,
                          ),

                          child: new SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            reverse: true,

                            // here's the actual text box
                            child: new TextField(
                              keyboardType: TextInputType.multiline,
                              maxLines: null, //grow automatically
                              controller: _textController,
//                              onSubmitted: currentIsComposing ? _handleSubmitted : null,
                              decoration: new InputDecoration.collapsed(
                                hintText: 'Your Question',
                              ),
                            ),
                            // ends the actual text box

                          ),



                        ),
                      )
                  ),
                  /* new Icon(Icons.zoom_out_map, color: Colors.grey, size: 18)*/
                ],
              ),
            ),
            new Row(
              children: <Widget>[
                Container(width: 10),
                Icon(Icons.insert_emoticon, color: Colors.grey, size: 18),
                /*Container(width: 10),
                Icon(Icons.attach_file, color: Colors.grey, size: 18),
                Container(width: 10),
                Icon(Icons.photo_camera, color: Colors.grey, size: 18),*/
                Spacer(),
                FlatButton(
                  onPressed: ()  async {


//                    check if

                  /*if(!sending){

                    if(_textController.text.trim().length > 0){

                      setState(() {
                        sending= true;
                      });


   *//* List p = json.decode(await Util.getPrefString(Util.LAST_LOGIN_USER));
    var params = p.elementAt(0);
    String token = await Util.getUserToken();



    String res = await HttpUtil.post(
    URLS.addquery,{
    'fromid':token,
    'message':_textController.text.trim()
    },with_auth: false
    );



    if(res=="done"){
      _textController.clear();
      setState(() {
        sending= false;
      });



    }*//*






   *//* try {

    var a = json.decode(res);

    items = a['items'];
    setState(() {

    });

    } on FormatException catch (e) {

    }*//*




                    } else
                      {
                        Util.snack(context, "Please Enter message");
                      }
                  }*/


                  },
                  color: Colors.green,
                  textColor: Colors.white,
                  child: Row( // Replace with a Row for horizontal icon + text
                    children: <Widget>[
                      Text((sending?"Sending...":"Send")),
                      Container(width: 10),
                      Icon(Icons.send, color: Colors.white, size: 18)
                    ],
                  ),
                ),
                Container(width: 10)
              ],
            ),
          ],
        )
    );
  }
}

