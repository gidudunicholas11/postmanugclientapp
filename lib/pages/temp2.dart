

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:postmanug/components/exp_info_tile.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/widgets/app-bar.dart';
import 'package:postmanug/widgets/custom-button.dart';

class Temp2 extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Size size = MediaQuery.of(context).size;


    return Scaffold(
//      backgroundColor: MyTheme.primaryColor,

    appBar: CustomAppBar(
      title_str: "User Agreement",
      has_icon: true,
      custom_icon: null,
      onBackIconPressed: null,
    ),

      body:Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[

//                  Spacer(),
                 Padding(
              padding: EdgeInsets.all(16),
              child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[



                  Text(
                    "The User Agreement",
                    style: TextStyles.airbnbCerealMedium.copyWith(
                        color: MyTheme.milanoRed,
                        fontSize: MyTheme.heading1
                    ),
                  ),
                  SizedBox(height: 5,),

                  Text(
                    "PLEASE READ CAREFULLY BEFORE USING THE POSTMAN APP",
                    style: TextStyles.airbnbCerealBook.copyWith(
                        color: MyTheme.black,
                        fontSize: MyTheme.heading3
                    ),
                  ),




                  ExpandedInfoTile.buildExpandedTiles(
                      <TileInfo>[

                        new TileInfo(
                          "1. Lorem Ipsum",
                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui offi",
                        ),

                        new TileInfo(
                          "2. Lorem Ipsum",
                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui offi",

                        ),

                        new TileInfo(
                          "3. Lorem Ipsum",
                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui offi",

                        ),

                        new TileInfo(
                          "4. Lorem Ipsum",
                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui offi",

                        ),

                        new TileInfo(
                          "5. Lorem Ipsum",
                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui offi",

                        )





                      ]
                  ),


                  SizedBox(
                    height: 100,
                  )



                ],
              )
              ,
            ),





              ],
            ),
          ),

          new Positioned(
            child: new Align(
              alignment: Alignment.bottomCenter,
              child:


              Stack(
                children: <Widget>[
                  ShaderMask(
                      shaderCallback: (rect) {
                        return LinearGradient(
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                          colors: [Colors.black, Colors.transparent],
                        ).createShader(Rect.fromLTRB(0, 0, rect.width, rect.height));
                      },
                      blendMode: BlendMode.dstIn,
                      child:

                      Container(
                        padding: EdgeInsets.all(20),
                        width: size.width-20,
                        color: Colors.white,
                        child:
                          SizedBox(
                            height: 50,
                          )

                      )
                  ),

                  CustomButton(
                    title: "I AGREE",
                    onPressed: (){
                    },
                  )


                ],
              )


            ),
          )
        ],
      )
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

}



