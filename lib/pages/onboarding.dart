

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:postmanug/pages/user-agreement.dart';
import 'package:postmanug/util/theme.dart';

class OnBoarding extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: MyTheme.primaryColor,


      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent
        child: IntroductionScreen(
          pages: listPagesViewModel(),
          onDone: () {
            // When done button is press

            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => UserAgreement()));


          },
          onSkip: () {
            // You can also override onSkip callback
          },
//          showSkipButton: true,
//          skip: const Icon(Icons.skip_next),
//          next: const Icon(Icons.skip_next),
          done: const Text("Done", style: TextStyle(fontWeight: FontWeight.w600)),
          dotsDecorator: DotsDecorator(
              size: const Size.square(10.0),
              activeSize: const Size(20.0, 10.0),
              activeColor: MyTheme.colorAccent,
              color: Colors.black26,
              spacing: const EdgeInsets.symmetric(horizontal: 3.0),
              activeShape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0)
              )
          ),
        )

      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  List<PageViewModel>listPagesViewModel() {
    return [
      PageViewModel(
        title: "Postman delivers your\ngoods to the\nlast mile",
        body: "Simply follow our registration process to generate your national Postcode and Address for your home or workplace.",
        image:  Center(child:
            new Image.asset(
                "images/onboarding1.png"
            )

        ),

        decoration: const PageDecoration(
          titleTextStyle: TextStyle(
              color: Colors.black, fontSize: 25,
            fontWeight: FontWeight.bold
          ),
          bodyTextStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18.0),

        ),
      ),

      PageViewModel(
        title: "Postman delivers your\ngoods to the\nlast mile",
        body: "Simply follow our registration process to generate your national Postcode and Address for your home or workplace.",
        image:  Center(child:
        new Image.asset(
            "images/onboarding2.png"
        )

        ),

        decoration: const PageDecoration(
          titleTextStyle: TextStyle(
              color: Colors.black, fontSize: 25,
              fontWeight: FontWeight.bold
          ),
          bodyTextStyle: TextStyle(fontWeight: FontWeight.w500, fontSize: 18.0),

        ),
      ),
    ];
  }

}