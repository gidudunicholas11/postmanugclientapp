import 'dart:convert';

import 'package:dropdown_menu/dropdown_menu.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:image_picker/image_picker.dart';
//import 'package:image_selector_formfield/image_selector_formfield.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/util/CustomShapeClipper.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/util/validator.dart';
import 'package:postmanug/widgets/app-bar.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/date-time-pickers.dart';
import 'package:postmanug/widgets/inline-double-display.dart';
import 'package:postmanug/widgets/text-input-field.dart';

class PackageDetailsScreen extends StatefulWidget {
  var pkg_details;
  var image_str;
  var origin;
  var destination;
  PackageDetailsScreen(
      {Key key,
      this.pkg_details,
      this.image_str,
      this.origin,
      this.destination})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PackageDetailsScreenState();
  }
}

enum AccountType { individual, business }

class PackageDetailsScreenState extends State<PackageDetailsScreen> {
  var image_file = null;

  var image_base64 = null;

  String price = "230,000 UGX - 290,000 UGX";
  TextEditingController _controller_length;
  TextEditingController _controller_width;
  TextEditingController _controller_height;
  TextEditingController _controller_weight;
  TextEditingController _controller_additional;
  TextEditingController _controller_name;
  TextEditingController _controller_quantity;

  String initial = 'Estimate Price';

  String packageid = '';

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.pkg_details == null) {
      _controller_length = TextEditingController();
      _controller_width = TextEditingController();
      _controller_height = TextEditingController();
      _controller_weight = TextEditingController();
      _controller_additional = TextEditingController();
      _controller_name = TextEditingController();
      _controller_quantity = TextEditingController();
    } else {
      _controller_length =
          TextEditingController(text: widget.pkg_details['length']);
      _controller_width =
          TextEditingController(text: widget.pkg_details['width']);
      _controller_height =
          TextEditingController(text: widget.pkg_details['height']);
      _controller_weight =
          TextEditingController(text: widget.pkg_details['weight']);
      _controller_additional = TextEditingController(
          text: widget.pkg_details['additional_information']);
      _controller_name =
          TextEditingController(text: widget.pkg_details['name']);
      _controller_quantity =
          TextEditingController(text: widget.pkg_details['quantity']);

//      a['image']=image_base64;
    }
  }

//  final snackBar = SnackBar(
//    content: Text('Invalid Credentials!'),
//    backgroundColor: MyTheme.milanoRed,
//    action: SnackBarAction(
//      label: '',
//      onPressed: () {
//        // Some code to undo the change.
//      },
//    ),
//  );

  /*@override
  Future<void> initState()  {
    if()





    super.initState();
  }
*/
  @override
  void dispose() {
    super.dispose();
  }

  final _scaffoldKeys = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKeys,
      appBar: CustomAppBar(
        title_str: "Package Details",
        has_icon: true,
        custom_icon: null,
        onBackIconPressed: null,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: ListView(
              children: <Widget>[
                SizedBox(
                  height: 20,
                ),
                Text(
                  "Upload Image of the Package",
                  textAlign: TextAlign.center,
                  style: TextStyles.airbnbCerealBook.copyWith(
                    fontSize: 17,
                    color: MyTheme.black,
                  ),
                ),
                Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    new Container(
                      margin: const EdgeInsets.all(16),
                      padding: const EdgeInsets.all(0),
                      decoration: BoxDecoration(
                          border: Border.all(color: MyTheme.milanoRed)),
                      child: Container(
                        color: MyTheme.light_grey,
                        height: 200,
                        child: (image_file != null)
                            ? Image.file(image_file)
                            : Container(),
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: new ButtonBar(
                        alignment: MainAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                            child: Icon(
                              Icons.add_photo_alternate,
                              size: 40,
                              color: MyTheme.milanoRed,
                            ),
                            onTap: () async {
                              var image = await ImagePicker.pickImage(
                                  source: ImageSource.gallery);

                              if (image != null) {
                                image_file = image;

                                setState(() {});

                                List<int> imageBytes =
                                    image_file.readAsBytesSync();
//                  print(imageBytes);

                                String base64Image = base64Encode(imageBytes);

                                this.image_base64 = base64Image;
//

                              }
                            },
                          )
                        ],
                      ),
                    ),
                    Positioned(
                      bottom: 35,
                      right: 145,
                      child: GestureDetector(
                        onTap: () async {
                          var image = await ImagePicker.pickImage(
                            source: ImageSource.camera,
                          );

                          if (image != null) {
                            image_file = image;

                            setState(() {});

                            List<int> imageBytes = image_file.readAsBytesSync();
//                  print(imageBytes);

                            String base64Image = base64Encode(imageBytes);

                            this.image_base64 = base64Image;
//

                          }
                        },
                        child: Icon(
                          Icons.camera,
                          color: MyTheme.milanoRed,
                          size: 33.0,
                        ),
                      ),
                    ),
                  ],
                ),
                Text(
                  "Set estimated package details",
                  textAlign: TextAlign.center,
                  style: TextStyles.airbnbCerealBook.copyWith(
                    fontSize: 17,
                    color: MyTheme.black,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SvgPicture.asset(
                  "assets/images/package.svg",
                  width: 100,
                  height: 100,
                  color: MyTheme.milanoRed,
                ),
                SizedBox(
                  height: 10,
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Text(
                        "Package Name",
                        textAlign: TextAlign.center,
                        style: TextStyles.airbnbCerealBook.copyWith(
                          fontSize: 17,
                          color: MyTheme.black,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MyTextInputField(
                        hint: "Package Name",
                        show_label: false,
                        Label: "",
                        minLine: 1,
                        maxLine: 1,
                        controller: _controller_name,
                        validator: (value) {
                          return Validator().validateRequired(value);
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Quantity",
                        textAlign: TextAlign.center,
                        style: TextStyles.airbnbCerealBook.copyWith(
                          fontSize: 17,
                          color: MyTheme.black,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MyTextInputField(
                        hint: "Quantity",
                        show_label: false,
                        Label: "",
                        minLine: 1,
                        maxLine: 1,
                        controller: _controller_quantity,
                        validator: (value) {
                          return Validator().validateRequired(value);
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      new Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Expanded(
                            child: new Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: MyTextInputField(
                                hint: "",
                                show_label: true,
                                Label: "Length(cm)",
                                controller: _controller_length,
                                validator: (value) {
                                  return Validator().validateRequired(value);
                                },
                              ),
                            ),
                          ),
                          Expanded(
                            child: new Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: MyTextInputField(
                                hint: "",
                                show_label: true,
                                Label: "Width(cm)",
                                controller: _controller_width,
                                validator: (value) {
                                  return Validator().validateRequired(value);
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      new Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Expanded(
                            child: new Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: MyTextInputField(
                                hint: "",
                                show_label: true,
                                Label: "Height(cm)",
                                controller: _controller_height,
                                validator: (value) {
                                  return Validator().validateRequired(value);
                                },
                              ),
                            ),
                          ),
                          Expanded(
                            child: new Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: MyTextInputField(
                                hint: "",
                                show_label: true,
                                Label: "Weight(Kg)",
                                controller: _controller_weight,
                                validator: (value) {
                                  return Validator().validateRequired(value);
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Additional Information",
                        textAlign: TextAlign.center,
                        style: TextStyles.airbnbCerealBook.copyWith(
                          fontSize: 17,
                          color: MyTheme.black,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      MyTextInputField(
                        hint: "",
                        show_label: false,
                        Label: "",
                        minLine: 5,
                        maxLine: 5,
                        controller: _controller_additional,
                        validator: (value) {
                          return Validator().validateRequired(value);
                        },
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
                Text(
                  "Price Estimate",
                  textAlign: TextAlign.center,
                  style: TextStyles.airbnbCerealBook.copyWith(
                    fontSize: 17,
                    color: MyTheme.black,
                  ),
                ),
                CustomButton(
//                title: price,
                  title: initial,
                  onPressed: () async {
                    _scaffoldKeys.currentState.showSnackBar(new SnackBar(
                      duration: new Duration(seconds: 4),
                      content: new Row(
                        children: <Widget>[
                          new CircularProgressIndicator(),
                          new Text("  Estimating price")
                        ],
                      ),
                    ));

                    String res = await HttpUtil.post(
                        URLS.clientprofile +
                            (await Util.getPrefString(Util.CLIENT_ID)
                                as String) +
                            "/packages/",
                        {
                          'origin_coordinates': widget.origin,
                          'package_weight': _controller_weight.text,
                          'package_dimensions': '9x3x2',
                          'package_name': _controller_name.text,
                          'package_description': _controller_additional.text,
                          'package_picture': image_base64,
                          'quantity': _controller_quantity.text,
                          'destination_address': widget.destination,
                          'weight_unit': '6417885661295153642',
                          'dimensions_unit': '7107823495418089962',
                        },
                        with_auth: true);

                    var e = jsonDecode(res);

                    print(e);
                    if (res.contains('cost_estimate')) {
                      print(e['status']);
                      print(e['cost_estimate']);
                      setState(() {
                        //
                        //int.parse(e['cost_estimate']).toString();
                        initial = e['cost_estimate'].toString();
                        packageid = e['package_id'].toString();
                      });
                    } else {
                      print("no it doesn't");
                    }

                    /*Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RequestVerification()));*/
                  },
                ),
                RaisedButton(
                  textColor: Colors.white,
                  color: MyTheme.milanoRed,
                  child: Padding(
                    padding: EdgeInsets.all(20),
                    child: Text(
                      'CONFIRM',
                      style: TextStyles.airbnbCerealBook
                          .copyWith(color: Colors.white, fontSize: 18),
                    ),
                  ),
                  onPressed: () {
                    if (image_file == null) {
//                    final snackBar = SnackBar(content: Text('Please take a picture of the package'));
//                    Scaffold.of(context).showSnackBar(snackBar);
                      Util.basicAlert(
                          context, 'Please take a picture of the package');
                    } else if (_formKey.currentState.validate()) {
                      Map a = new Map();

                      a['length'] = _controller_length.text;
                      a['width'] = _controller_width.text;
                      a['package_id'] = packageid;
                      a['height'] = _controller_height.text;
                      a['weight'] = _controller_weight.text;
                      a['additional_information'] = _controller_additional.text;
                      a['quantity'] = _controller_quantity.text;
                      a['name'] = _controller_name.text;
                      a['image'] = image_base64;
                      Navigator.pop(context, json.encode(a));

                      print(a);
                    } else {}
                  },
                  /* shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(18.0),
                    side: BorderSide(color: MyTheme.milanoRed)
                ),*/
                )
              ],
            )),
      ),
    );
  }
}
