import 'dart:convert';
import 'dart:ffi';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/pages/add-business-profile.dart';
import 'package:postmanug/pages/home.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/widgets/app-bar.dart';
import 'package:postmanug/widgets/custom-button.dart';

import 'add-individual-profile.dart';


class VerificationCodeScreen extends StatefulWidget{
   String phoneNumber;

   VerificationCodeScreen({Key key, this.phoneNumber}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return VerificationCodeScreenState();
  }



}


enum AccountType { individual, business }

class VerificationCodeScreenState extends State<VerificationCodeScreen>{
  AccountType _account_type = AccountType.individual;


  final _formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  int _radioValue1 = 1;

  bool valid = false;


  var onTapRecognizer;

  /// this [StreamController] will take input of which function should be called

  bool hasError = false;
  String currentText = "";


  static String phoneNumber = "";

  TextEditingController  pin_controller;
  bool is_sending = false;


  @override
  void initState() {
    onTapRecognizer = TapGestureRecognizer()
      ..onTap = () {
//        Navigator.pop(context);
      };

    pin_controller = new TextEditingController();

    getPhoneNumber();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      key: scaffoldKey,
      appBar: CustomAppBar(
        title_str: "Input Verification Code",
        has_icon: true,
        custom_icon: null,
        onBackIconPressed: null,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ListView(
            children: <Widget>[
              SizedBox(height: 30),
              Image.asset(
                'assets/images/app_icon.png',
                height: MediaQuery.of(context).size.height / 3,
                fit: BoxFit.fitHeight,
              ),
              SizedBox(height: 8),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  'Phone Number Verification',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8),
                child: RichText(
                  text: TextSpan(
                      text: "Enter the code sent to ",
                      children: [
                        TextSpan(
                            text: widget.phoneNumber,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15)),
                      ],
                      style: TextStyle(color: Colors.black54, fontSize: 15)),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                  padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 30),
                  child: PinCodeTextField(
                    length: 4,
                    controller: pin_controller,
                    obsecureText: false,
                    animationType: AnimationType.fade,
                    shape: PinCodeFieldShape.underline,
                    animationDuration: Duration(milliseconds: 300),
                    borderRadius: BorderRadius.circular(5),
                    fieldHeight: 50,
                    fieldWidth: 40,
                    onChanged: (value) {
                      setState(() {
                        currentText = value;
                      });
                    },
                  )),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                // error showing widget
                child: Text(
                  hasError ? "*Please fill up all the cells properly" : "",
                  style: TextStyle(color: Colors.red.shade300, fontSize: 15),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                    text: "Didn't receive the code? ",
                    style: TextStyle(color: Colors.black54, fontSize: 15),
                    children: [
                      TextSpan(
                          text: " RESEND",
                          recognizer: onTapRecognizer,

                          style: TextStyle(
                              color: Color(0xFF91D3B3),
                              fontWeight: FontWeight.bold,
                              fontSize: 16))
                    ]),
              ),
              SizedBox(
                height: 14,
              ),
              CustomButton2(
                title: "VERIFY",
                backgroundColor: is_sending?Colors.grey:MyTheme.primaryColor,
                onPressed: () async {
                 /* Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => HomePage(
//                        phoneNumber: "+256706080109",
                      )));*/



                  if(currentText.length==4){

                    setState(() {
                      is_sending = true;
                    });
                    String reg_id = await Util.getPrefString(Util.REG_ID) ;

                    print(URLS.registrations+reg_id+"/verify_code/");
                    String res = await HttpUtil.post(
                        URLS.registrations+reg_id+"/verify_code/",{
                      'verification_code':pin_controller.text,

                    }
                    );

                    setState(() {
                      is_sending = false;
                    });

                 print(res);

                    try {
                      var x = json.decode(res);


                      if(x.containsKey("status_message")){

                        if((x['status_message'] == "Verification succeeded.") || (x['status_message'] == "Already verified.")){

//                          try to login now

                        loginUser();
                        }else{
                          Util.basicAlert(context,"Verification Failed");
//                          loginUser();
                        }




                      }else{

                        Util.basicAlert(context,"Error while getting code, try again");
                      }
                    } on FormatException catch (e) {
                      Util.snack(context,"Error while getting code, try again");
                    }
                  }
                },

              ),
            ],
          ),
        ),
      ),
    );
  }

   Future<Void> getPhoneNumber() async {
     String num = await Util.getPrefString(Util.ACCOUNT_PHONE_PREF);
     setState(() {
       phoneNumber = num;
     });
  }

  Future<void> loginUser() async {

    String res = await HttpUtil.post(
        URLS.login,{
      'username':phoneNumber,
      'password':currentText,

    }
    );

    print(res);

    try {
      var x = json.decode(res);


      if(x.containsKey("token")){

       Util.savePrefString(Util.USER_TOKEN, x['token']);
       Util.savePrefString(Util.PKXNUM,currentText);
       Util.savePrefString(Util.CLIENT_ID, x['profile']['client_id']);
       Util.savePrefString(Util.LAST_LOGIN_USER, res);

//       Util.savePrefString(Util.USER_LOGGED_IN, "true");

       Navigator.pop(context);

       if (await Util.getPrefString(Util.ACCOUNT_TYPE_PREF) as String == "1"){
         Navigator.push(
             context,
             MaterialPageRoute(builder: (context) => AddIndividualProfileScreen(
             )));
       }else{
         Navigator.push(
             context,
             MaterialPageRoute(builder: (context) => AddBusinessProfileScreen(
             )));
       }





      }else{
        Util.basicAlert(context,"Error while logging, try again");
      }
    } on FormatException catch (e) {
      Util.snack(context,"Error while getting code, try again");
    }



  }
}
