
import 'dart:async';
import 'dart:convert';

import 'package:dropdown_menu/dropdown_menu.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:geolocator/geolocator.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:image_picker/image_picker.dart';
//import 'package:image_selector_formfield/image_selector_formfield.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/pages/generate-address-code.dart';
import 'package:postmanug/pages/generate-address-code_new.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/util/validator.dart';
import 'package:postmanug/widgets/app-bar.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/date-time-pickers.dart';
import 'package:postmanug/widgets/text-input-field.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'home.dart';


class AddBusinessProfileScreen extends StatefulWidget{
  final String phoneNumber;

  const AddBusinessProfileScreen({Key key, this.phoneNumber}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AddBusinessProfileScreenState();
  }

}


enum AccountType { individual, business }

class AddBusinessProfileScreenState extends State<AddBusinessProfileScreen>{
  AccountType _account_type = AccountType.individual;


  final _formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  int _radioValue1 = 1;

  String phoneNumber = '';
  bool valid = false;


  var onTapRecognizer;

  /// this [StreamController] will take input of which function should be called

  bool hasError = false;
  String currentText = "";
  String imageData1 = "";

  Map user_item={'profile': {'user': {'username': "", 'first_name': "", 'last_name': "", 'email':"" }, 'date_of_birth': "", 'gender': "", 'phone_number': "", 'photo': 'http://54.186.202.6:9015/media/photos/clients/no-img.jpg', 'client_uuid': '', 'created_at': '', 'post_office_box': '', 'physical_address': '', 'device_id': '', 'client_id': '', 'default_pin_activated': '', 'account_type': ''}, 'token': ''};



  List<Map<String, dynamic>> TYPES = [
    {"title": "全部", "id": 0},
    {"title": "甜点饮品", "id": 1},
    {"title": "生日蛋糕", "id": 2},
    {"title": "火锅", "id": 3},
    {"title": "自助餐", "id": 4},
    {"title": "小吃", "id": 5},
    {"title": "快餐", "id": 6},
    {"title": "日韩料理", "id": 7},
    {"title": "西餐", "id": 8},
    {"title": "聚餐", "id": 9},
    {"title": "烧烤", "id": 10},
    {"title": "川菜", "id": 11},
    {"title": "江浙菜", "id": 12},
    {"title": "东北菜", "id": 13},
    {"title": "蒙餐", "id": 14},
    {"title": "新疆菜", "id": 15},
  ];

   int TYPE_INDEX = 2;

   var image_file = null;
   String image_base64 = null;


  Position _currentPosition;


  var img_base64 = null;

  String gender = null;


  List zones_list = new List();
  List district_list = new List();
  List county_list = new List();
  List sub_county_list = new List();
  List parish_list = new List();

  String zone, district, county, sub_county, parish;

  TextEditingController _controller_first_name = TextEditingController();
  TextEditingController _controller_company_name = TextEditingController();
  TextEditingController _controller_position = TextEditingController();
  TextEditingController _controller_last_name = TextEditingController();
  TextEditingController _controller_email = TextEditingController();
  TextEditingController _controller_dob = TextEditingController();
  TextEditingController _controller_p_o_box = TextEditingController();
  TextEditingController _controller_plot_number = TextEditingController();
  TextEditingController controller_physical_address = TextEditingController();
  TextEditingController controller_street = TextEditingController();



  @override
  void initState()  {
    super.initState();
    onTapRecognizer = TapGestureRecognizer()
      ..onTap = () {
//        Navigator.pop(context);
      };

    getUser();

    getAssetImages();



    _currentPosition = Position(longitude: 0, latitude: 0);

//    check user exists

//  checkUser();
    _getCurrentLocation();
    getZones();
  }
  Future<void> getUser() async {



    String res = await HttpUtil.get(
        URLS.clientprofile+(await Util.getPrefString(Util.CLIENT_ID) as String)+"/", with_auth: true
    );


    var u = json.decode(res);
    user_item = u;


    _controller_first_name.text=u['user']['first_name'];
//    _controller_company_name.text=u['user']['business_info'];
//    _controller_position.text=u['user']['first_name'];
    _controller_last_name.text=u['user']['last_name'];
    _controller_email.text=u['user']['email'];
    _controller_dob.text=u['date_of_birth'];
    _controller_p_o_box.text=u['post_office_box'];
//    _controller_plot_number.text=u['user'];
    controller_physical_address.text=u['physical_address'];
//    controller_street.text=u['user'];

    gender = u['gender'];

    setState(() {

    });





  }
  @override
  void dispose() {
    super.dispose();
  }
  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator();

    var locationOptions = LocationOptions(
        accuracy: LocationAccuracy.high, distanceFilter: 10);

    StreamSubscription<Position> positionStream = geolocator.getPositionStream(
        locationOptions).listen(
            (Position position) {
          _currentPosition = position;
          setState(() {

          });
        });
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;

    return Scaffold(

      key: scaffoldKey,
      appBar: CustomAppBar(
        title_str: "Add Business Profile",
        has_icon: true,
        custom_icon: null,
        onBackIconPressed: null,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
//    Form(
//    key: _formKey,
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,

          child: Form(
            key: _formKey,
            child: ListView(

            children: <Widget>[


              Center(
                  child:
                  Container(
                    width: 160,
                    height: 160,
                    child: Stack(
                      children: <Widget>[

//                        new CircleAvatar(radius:_width<_height? _width/4:_height/4,backgroundImage: !image_picked?NetworkImage(Util.getDPImageUrl(widget.item['dp'])):FileImage(image_file),),

                        CircleAvatar(
                          backgroundImage: (image_file!=null)?FileImage(image_file):(user_item['photo']==null?null:NetworkImage(user_item['photo'])),
                          radius: 80,

                        ),


                        Align(
                          alignment: Alignment.bottomRight,
                          child: new ButtonBar(
                            alignment: MainAxisAlignment.end,
                            children: <Widget>[
                              /*Container(
                              width: 180,
                              height: 180,
                              child: ImageSelectorFormField(
                                cropMaxWidth: 100,
                                cropMaxHeight: 100,
                                icon: Icon(Icons.add_photo_alternate,size: 50,color: Colors.green,),
                                cropStyle: CropStyle.circle,
                              ),
                            )*/

                              InkWell(
                                child:Icon(Icons.add_photo_alternate, size: 40, color: MyTheme.milanoRed,),
                                onTap: () async {
                                  var image = await ImagePicker.pickImage(source: ImageSource.gallery);

                                  if(image!=null){
                                    image_file = image;

                                    setState(() {


                                    });


                                    List<int> imageBytes = image_file.readAsBytesSync();
//                  print(imageBytes);

                                    String base64Image = base64Encode(imageBytes);

                                    this.image_base64 = base64Image;
//


                                  }
                                },
                              )


                            ],
                          ),
                        ),






                      ],
                    ),
                  )


              ),

              SizedBox(height: 10,),


              Text(
                "Personal Information",
                textAlign: TextAlign.center,

                style: TextStyles.airbnbCerealMedium.copyWith(
                  fontSize: 20,
                  color: MyTheme.black,

                ),
              ),

              Text("Please ensure all information is input\n correctly and all tabs filled in before you\n submit the form.",
                textAlign: TextAlign.center,

                style: TextStyles.airbnbCerealBook.copyWith(
                  fontSize: MyTheme.heading3,
                  color: MyTheme.black,

                ),
              ),

              SizedBox(height: 20,),

              MyTextInputField(
                hint: "Enter company name",
                show_label: true,
                Label: "Name of Company",

                controller: _controller_company_name,
                validator: (value){
                  return null;
                },

              ),

              SizedBox(height: 20,),


              Text(
                "Registrant Information",
                textAlign: TextAlign.center,

                style: TextStyles.airbnbCerealMedium.copyWith(
                  fontSize: 20,
                  color: MyTheme.black,

                ),
              ),

              Text("Details of the user registering on behalf of \nthe company.",
                textAlign: TextAlign.center,

                style: TextStyles.airbnbCerealBook.copyWith(
                  fontSize: MyTheme.heading3,
                  color: MyTheme.black,

                ),
              ),

              SizedBox(height: 20,),

              MyTextInputField(
                hint: "Enter position of registrant ie Managing Director",
                show_label: true,
                Label: "Position",

                controller: _controller_position,
                validator: (value){
                  return null;
                },

              ),

              SizedBox(height: 20,),

              MyTextInputField(
                hint: "Enter your first name",
                show_label: true,
                Label: "First Name",

                controller: _controller_first_name,
                validator: (value){
                  return Validator().validateRequired(value);
                },

              ),



              MyTextInputField(
                hint: "Enter your last name",
                show_label: true,
                Label: "Last Name",
                controller: _controller_last_name,
                validator: (value){
                  return Validator().validateRequired(value);
                },

              ),

              MyTextInputField(
                hint: "email@example.com",
                show_label: true,
                Label: "Email",
                controller: _controller_email,
                validator: (value){
                  return Validator().validateRequired(value);
                },

              ),

              BasicDateField(
                label: "Date of Birth",
                controller: _controller_dob,
                validator: (DateTime value){
                  print(value.toString());
                  return Validator().validateRequired(value.toString());
                },

              ),

              Padding(padding: EdgeInsets.only(left: 16),
                child: Text(
                  "Gender",
                  textAlign: TextAlign.left,
                  style: TextStyles.airbnbCerealBook.copyWith(
                      color: Colors.black,
                      fontSize: MyTheme.heading4
                  ),
                ),
              ),
              RadioButtonGroup(
                  labels: <String>[
                    "M",
                    "F",
                  ],
                  picked: gender,
                  onSelected: (String selected) {
                    gender = selected;
                    setState(() {

                    });
                  }
              ),


              SizedBox(height: 10,),


              Text(
                "National Postcode",
                textAlign: TextAlign.center,

                style: TextStyles.airbnbCerealMedium.copyWith(
                  fontSize: 20,
                  color: MyTheme.black,

                ),
              ),

              Text("Based on the location of the business.",
                textAlign: TextAlign.center,

                style: TextStyles.airbnbCerealBook.copyWith(
                  fontSize: MyTheme.heading3,
                  color: MyTheme.black,

                ),
              ),

              SizedBox(height: 20,),

              new Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Expanded(
                    child: new Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: MyTextInputField(
                        hint: "Enter P.O.Box",
                        show_label: true,
                        Label: "P.O.Box",
                        controller: _controller_p_o_box,
                        validator: (value){
                          return Validator().validateRequired(value);
                        },

                      ),
                    ),
                  ),
                  Expanded(
                    child: new Padding(
                      padding: const EdgeInsets.all(0.0),
                      child: MyTextInputField(
                        hint: "Enter plot number",
                        show_label: true,
                        Label: "Plot Number",
                        controller: _controller_plot_number,
                        validator: (value){
                          return Validator().validateRequired(value);
                        },

                      ),
                    ),
                  ),
                ],
              ),

              MyTextInputField(
                hint: "Street",
                show_label: true,
                Label: "Street",
                controller: controller_street,
                validator: (value) {
                  return Validator().validateName(value);
                },

              ),


              MyTextInputField(
                hint: "Physical Address",
                show_label: true,
                Label: "Physical Address",
                controller: controller_physical_address,
                validator: (value) {
                  return Validator().validateName(value);
                },

              ),




              SizedBox(height: 20,),

              new Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Expanded(
                    child: new Column(
                      children: <Widget>[


                        Text(
                          "Zones",
                          textAlign: TextAlign.left,
                          style: TextStyles.airbnbCerealBook
                              .copyWith(
                              color: Colors.black,
                              fontSize: MyTheme.heading4
                          ),
                        ),

                        Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 10.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  15.0),
                              border: Border.all(
                                  color: Colors.grey,
                                  style: BorderStyle.solid,
                                  width: 0.80),
                            ),
                            width: size.width * 0.45,
                            child: DropdownButton<String>(

                              value: zone,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(
                                  color: Colors.black
                              ),
                              underline: Container(
                                height: 2,
                                color: Colors.transparent,
                              ),
                              onChanged: (String newValue) {
                                var item = json.decode(newValue);
                                zone = newValue;
                                setState(() {


                                });
//                                     print(item['districts']);

                                getDistricts(item['districts']);
                              },

                              items: zones_list
                                  .map<DropdownMenuItem<String>>((
                                  var item) {
                                return DropdownMenuItem<String>(
                                  value: json.encode(item),
                                  child: Text(item['name']),
                                );
                              })
                                  .toList(),
                            )
                        ),


                      ],
                    ),
                  ),

                  Expanded(
                    child: new Column(
                      children: <Widget>[


                        Text(
                          "Districts",
                          textAlign: TextAlign.left,
                          style: TextStyles.airbnbCerealBook
                              .copyWith(
                              color: Colors.black,
                              fontSize: MyTheme.heading4
                          ),
                        ),

                        Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 10.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  15.0),
                              border: Border.all(
                                  color: Colors.grey,
                                  style: BorderStyle.solid,
                                  width: 0.80),
                            ),
                            width: size.width * 0.45,
                            child: DropdownButton<String>(

                              value: district,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(
                                  color: Colors.black
                              ),
                              underline: Container(
                                height: 2,
                                color: Colors.transparent,
                              ),
                              onChanged: (String newValue) {
                                var item = json.decode(newValue);
                                district = newValue;
                                setState(() {


                                });


                                if(item['name']=="KAMPALA"){
                                  getCounties(item['divisions']);
                                }else{
                                  getCounties(item['counties']);
                                }


                              },

                              items: district_list
                                  .map<DropdownMenuItem<String>>((
                                  var item) {
                                return DropdownMenuItem<String>(
                                  value: json.encode(item),
                                  child: Text(item['name']),
                                );
                              })
                                  .toList(),
                            )
                        ),


                      ],
                    ),
                  ),


                ],
              ),
              SizedBox(height: 20,),
              new Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Expanded(
                    child: new Column(
                      children: <Widget>[


                        Text(
                          "County",
                          textAlign: TextAlign.left,
                          style: TextStyles.airbnbCerealBook
                              .copyWith(
                              color: Colors.black,
                              fontSize: MyTheme.heading4
                          ),
                        ),

                        Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 10.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  15.0),
                              border: Border.all(
                                  color: Colors.grey,
                                  style: BorderStyle.solid,
                                  width: 0.80),
                            ),
                            width: size.width * 0.45,
                            child: DropdownButton<String>(

                              value: county,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(
                                  color: Colors.black
                              ),
                              underline: Container(
                                height: 2,
                                color: Colors.transparent,
                              ),
                              onChanged: (String newValue) {
                                var item = json.decode(newValue);
                                setState(() {
                                  county = newValue;
                                });

//
                                var dist = json.decode(zone);
                                if(dist['name']=="KAMPALA"){
                                  parish = null;
                                  sub_county=null;
//                                              county=null;
                                  parish_list = new List();
                                  sub_county_list = new List();

                                  setState(() {

                                  });
                                  getParishes(
                                      item['parishes']);
                                }else{
                                  getSubCounties(
                                      item['sub_counties']);
                                }

//                                print(item);


                              },

                              items: county_list
                                  .map<DropdownMenuItem<String>>((
                                  var item) {
                                return DropdownMenuItem<String>(
                                  value: json.encode(item),
                                  child: Text(item['name']),
                                );
                              })
                                  .toList(),
                            )
                        ),


                      ],
                    ),
                  ),

                  Expanded(
                    child: new Column(
                      children: <Widget>[


                        Text(
                          "Sub County",
                          textAlign: TextAlign.left,
                          style: TextStyles.airbnbCerealBook
                              .copyWith(
                              color: Colors.black,
                              fontSize: MyTheme.heading4
                          ),
                        ),

                        Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 10.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  15.0),
                              border: Border.all(
                                  color: Colors.grey,
                                  style: BorderStyle.solid,
                                  width: 0.80),
                            ),
                            width: size.width * 0.45,
                            child: DropdownButton<String>(

                              value: sub_county,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(
                                  color: Colors.black
                              ),
                              underline: Container(
                                height: 2,
                                color: Colors.transparent,
                              ),
                              onChanged: (String newValue) {
                                var item = json.decode(newValue);
                                setState(() {
                                  sub_county = newValue;
                                });
//                                            print(item);

                                getParishes(item['parishes']);
                              },

                              items: sub_county_list
                                  .map<DropdownMenuItem<String>>((
                                  var item) {
                                return DropdownMenuItem<String>(
                                  value: json.encode(item),
                                  child: Text(item['name']),
                                );
                              })
                                  .toList(),
                            )
                        ),


                      ],
                    ),
                  ),




                ],
              ),
              SizedBox(height: 20,),

              new Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Expanded(
                    child: new Column(
                      children: <Widget>[


                        Text(
                          "Parishes",
                          textAlign: TextAlign.left,
                          style: TextStyles.airbnbCerealBook
                              .copyWith(
                              color: Colors.black,
                              fontSize: MyTheme.heading4
                          ),
                        ),

                        Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 10.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                  15.0),
                              border: Border.all(
                                  color: Colors.grey,
                                  style: BorderStyle.solid,
                                  width: 0.80),
                            ),
                            width: size.width * 0.45,
                            child: DropdownButton<String>(

                              value: parish,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(
                                  color: Colors.black
                              ),
                              underline: Container(
                                height: 2,
                                color: Colors.transparent,
                              ),
                              onChanged: (String newValue) {
                                var item = json.decode(newValue);
                                setState(() {
                                  parish = newValue;
                                });
//                                        print(item);

//                                        getSubCounties(item['sub_counties']);
                              },

                              items: parish_list
                                  .map<DropdownMenuItem<String>>((
                                  var item) {
                                return DropdownMenuItem<String>(
                                  value: json.encode(item),
                                  child: Text(item['name']),
                                );
                              })
                                  .toList(),
                            )
                        ),


                      ],
                    ),
                  ),




                ],
              ),

             /* SizedBox(height: 20,),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[

                  Text(
                      "Lat: " + _currentPosition.latitude.toString()
                  ),

                  SizedBox(
                    width: 20,
                  ),

                  Text(
                      "Long: " +
                          _currentPosition.longitude.toString()
                  )

                ],
              ),

              Center(
                child: Text(
                  "These coordinates represent the location of your address",
                  textAlign: TextAlign.center,

                ),
              ),*/

              Center(
                child: Text(
                  "This information will be verified by our Postman",
                  textAlign: TextAlign.center,

                ),
              ),

              CustomButton2(
                title: "Next",
                onPressed: () async {
                  /*if(_currentPosition.latitude==0 || _currentPosition.longitude==0){
                    Util.basicAlert(context, 'Location not yet picked');
                  }else*/
                  if(image_file==null && user_item['photo']==null){
                    Util.basicAlert(context, 'Please add profile picture');
                  }else if(gender==null){
                    Util.basicAlert(context, 'Please choose the gender');
                  }else
                  if(parish==null){
                    Util.basicAlert(context, 'Please make sure the parish is selected');
                  }else
                  if(_currentPosition.latitude==0 || _currentPosition.longitude==0){
                    Util.basicAlert(context, 'Location not yet picked');
                    _getCurrentLocation();
                  }else


                  if (_formKey.currentState.validate()) {
                    var body = {
                      'user_first_name':_controller_first_name.text,
                      'user_last_name':_controller_last_name.text,
                      'user_email':_controller_email.text,

                      'latitude':_currentPosition.latitude,
                      'longitude':_currentPosition.longitude,
//                      'map_code':_controller_first_name.text,
//                      'business_info':_controller_company_name.text,
                      'date_of_birth':_controller_dob.text,
                      'gender':gender,
                      'post_office_box	':_controller_p_o_box.text,
                      'physical_address':controller_physical_address.text,
//                      'device_id':_controller_first_name.text,
                      'account_type':2,
                    };

                    if(image_file!=null){
                      body['photo']=image_base64;
                    }
                    String res = await HttpUtil.patch(
                        URLS.clientprofile+(await Util.getPrefString(Util.CLIENT_ID) as String)+"/",body, with_auth: true
                    );

                    print(res);

                    Map x= json.decode(res);
                    if(x.containsKey('url')){
                      loginUser(x);
                    }else{
                      Util.basicAlert(context, "Error updating profile");
                    }

                  }else{
                  }
                },
              ),
              SizedBox(
                height: 10,
              ),









            ],
          ),
          )
        ),
      ),
    );
  }

  Future<void> getAssetImages() async {
    this.imageData1 = await rootBundle.load('images/user.png').toString();

    setState(() {

    });
  }
  Future<void> loginUser(var item) async {

    String res = await HttpUtil.post(
        URLS.login,{
      'username':item['user']['username'],
      'password':await Util.getPrefString(Util.PKXNUM),

    }
    );

    print(res);

    try {
      var x = json.decode(res);


      if(x.containsKey("token")){

        Util.savePrefString(Util.USER_TOKEN, x['token']);
        Util.savePrefString(Util.CLIENT_ID, x['profile']['client_id']);
        Util.savePrefString(Util.LAST_LOGIN_USER, res);

        Util.savePrefString(Util.USER_LOGGED_IN, "true");

//      check number of addresses

        String l = await HttpUtil.get(
            item['addresses'],with_auth: true
        );

        List list = json.decode(l);
        if(list.length==0 || list.length>0){

          var post_code = null;
          if(parish!=null){
            post_code = json.decode(parish)['post_code'];
          }
          var loc_item = {
            'latitude':_currentPosition.latitude,
            'longitude':_currentPosition.longitude,
            'post_office_box	':_controller_p_o_box.text,
            'plot_number	':_controller_plot_number.text,
            'physical_address':controller_physical_address.text,
            'national_post_code':post_code,
            'street':controller_street.text,
            'account_type':2,

          };
          Navigator.pop(context);
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => GenerateAddressCodeNew(
                item:loc_item
              )));
        }else{
           Navigator.pop(context);
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => HomePage(
            )));
        }







      }else{
        Util.basicAlert(context,"Error while logging, try again");
      }
    } on FormatException catch (e) {
      Util.snack(context,"Error while getting code, try again");
    }



  }


  Future<void> getZones() async {
    String res = await HttpUtil.get(
        URLS.zones, with_auth: true
    );

    print(res);

    county = null;
    district = null;
    sub_county=null;
    district_list=List();
    sub_county_list=List();
    parish=null;
    parish_list=List();
    county_list = new List();
    setState(() {

    });

    zones_list = json.decode(res);

    setState(() {

    });
  }

  Future<void> getDistricts(String url) async {
    print(url);

    String res = await HttpUtil.get(
        url, with_auth: true
    );

    county = null;
    district = null;
    sub_county=null;
    sub_county_list=List();
    parish=null;
    parish_list=List();
    county_list = new List();

    district_list = json.decode(res);
    setState(() {

    });
  }

  Future<void> getCounties(var url) async {
    String res = await HttpUtil.get(
        url, with_auth: true
    );

    sub_county = null;
    parish=null;
    parish_list=null;
    sub_county_list = new List();
    parish_list = List();

    county_list = json.decode(res);
    setState(() {

    });
  }

  Future<void> getSubCounties(var url) async {
    String res = await HttpUtil.get(
        url, with_auth: true
    );

    parish = null;
    sub_county=null;
    parish_list = new List();

    sub_county_list = json.decode(res);
    setState(() {

    });
  }

  Future<void> getParishes(var url) async {
    String res = await HttpUtil.get(
        url, with_auth: true
    );

    print(url);

    print(res);
    parish=null;
    parish_list = json.decode(res);

    setState(() {

    });
  }
}
