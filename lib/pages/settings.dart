
import 'package:dropdown_menu/dropdown_menu.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
//import 'package:image_selector_formfield/image_selector_formfield.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:postmanug/pages/add-address.dart';
import 'package:postmanug/pages/change-pin.dart';
import 'package:postmanug/util/CustomShapeClipper.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/widgets/app-bar.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/date-time-pickers.dart';
import 'package:postmanug/widgets/inline-double-display.dart';
import 'package:postmanug/widgets/text-input-field.dart';


class SettingsScreen extends StatefulWidget{

  const SettingsScreen({Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SettingsScreenState();
  }

}


enum AccountType { individual, business }

class SettingsScreenState extends State<SettingsScreen>{




  @override
  Future<void> initState()  {



    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: CustomAppBar(
        title_str: "Settings",
        has_icon: true,
        custom_icon: null,
        onBackIconPressed: null,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,

            child: ListView(
              children: <Widget>[
                new ListTile(
                    title: new Text("Change Pin"),
                    leading: new Icon(Icons.security, color: Colors.black,),
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new ChangePin()));
                    }
                ),

                Divider(
                ),

                new ListTile(
                    title: new Text("Add alternate address"),
                    leading: new Icon(Icons.add_location, color: Colors.black,),
                    onTap: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new AddAddress()));
                    }
                ),
                Divider(
                ),

                new ListTile(
                    title: new Text("Edit Current Address"),
                    leading: new Icon(Icons.edit_location, color: Colors.black,),
                    onTap: () {
                      /*Navigator.of(context).pop();
                      Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new OtherPage("First Page")));*/
                    }
                ),

              ],
            )

        ),
      ),
    );
  }

}
