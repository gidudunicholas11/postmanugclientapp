import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:loading/indicator/ball_spin_fade_loader_indicator.dart';
import 'package:loading/loading.dart';
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/pages/home.dart';
import 'package:postmanug/pages/onboarding.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/util/validator.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:postmanug/widgets/text-input-field.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'generate-address-code_new.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: MyTheme.milanoRed
      ),
      debugShowCheckedModeBanner: false,
      home: MyLogin(),
    );

  }
}

class MyLogin extends StatefulWidget {
  @override
  _MyLoginState createState() => _MyLoginState();
}

class _MyLoginState extends State<MyLogin> {

  TextEditingController _codecontroller = new TextEditingController();

  String phoneNumber = '';
  bool valid = true;

  handlesignIn() async {

    final snackBar = SnackBar(
      content: Text('Invalid Credentials!'),
      backgroundColor: MyTheme.milanoRed,
      action: SnackBarAction(
        label: '',
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );

    if(this._formKey.currentState.validate()){
      _scaffoldKey.currentState.showSnackBar(
          new SnackBar(duration: new Duration(seconds: 4), content:
          new Row(
            children: <Widget>[
              new CircularProgressIndicator(),
              new Text("  Signing-In...")
            ],
          ),
          ));
      var body = {
        'username': phoneNumber.replaceAll("+", ''),
        'password': _codecontroller.text,
      };
      String response = await HttpUtil.post(
          URLS.login+"/",body, with_auth: true
      );

      if(response.contains('Unable to log in with provided credentials.')){
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }else{
        try{

          var x = json.decode(response);

          // print(x);

          //if(response.contains('token'))
          if(response.contains("token")){

            Util.savePrefString(Util.USER_TOKEN, x['token']);
            Util.savePrefString(Util.CLIENT_ID, x['profile']['client_id']);
            Util.savePrefString(Util.LAST_LOGIN_USER, response);
            Util.savePrefString(Util.USER_LOGGED_IN, "true");

            print('Login In');



            print('ok response is'+ response);


            Navigator.push(

                context,
                MaterialPageRoute(builder: (context) => HomePage(
                )));

          } else{
            _scaffoldKey.currentState.showSnackBar(snackBar);

          }

        }on FormatException catch (e) {
          _scaffoldKey.currentState.showSnackBar(snackBar);
        }

      }


    }
  }

  void onPhoneNumberChanged(PhoneNumber phoneNumber) {
    print(phoneNumber);
    setState(() {
      this.phoneNumber = phoneNumber.toString();
    });
  }

  void onInputChanged(bool value) {
    print(value);
    setState(() {
      valid = value;
    });
  }

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: Icon(Icons.arrow_back),
        title: Text('Login'),
        centerTitle: true,

      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
             Container(
               height: MediaQuery.of(context).size.height/ 3.8,
               width: MediaQuery.of(context).size.width,
               child: Center(
                 child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(30.0)),
                     child: Image.asset('images/ic_icon_photos_v2_x2.png', height: 120,),),
               ),
             ),
              Form(
                key: _formKey,
                child: Container(
                  height: MediaQuery.of(context).size.height / 1.5,
                  width: double.infinity,
                  child: Column(
                    children: <Widget>[

                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10.0),
                        child: InternationalPhoneNumberInput.withCustomDecoration(
                            onInputChanged: onPhoneNumberChanged,
                            onInputValidated: onInputChanged,
                            countries:[
                              "UG"
                            ],
                            initialCountry2LetterCode: 'UG',
                            inputDecoration: InputDecoration(
                              hintText: 'Enter phone number',
                              errorText: valid ? null : 'Invalid',
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15),
                                ),
                              ),
                            )
                        ),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),

                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Container(
                          height: 100,
                          child: MyTextInputField(
                            hint: "Enter sms code",
                            show_label: true,
                            Label: "Code",
                             controller: _codecontroller,
                            validator: (value){
                              return Validator().validateRequired(value);
                            },

                          ),
                        ),
                      ),
                      SizedBox(height: 10.0,),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10.0),
                            child: RaisedButton(
                              color: MyTheme.milanoRed,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(20.0))
                              ),
                              onPressed: () async {

                                      handlesignIn();



                              },
                              child: Center(
                                child: Text('Have Account? Login', style: TextStyle(
                                    color: Colors.white
                                ),),
                              ),
                            ),
                          ),


                      Padding(
                        padding: const EdgeInsets.only(left: 120),
                        child: Container(
                          width: 150,
                          child: RaisedButton(
                            onPressed: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) =>
                                    OnBoarding()
                                ),
                              );
                            },
                            color: MyTheme.milanoRed,
                            child: Center(
                              child: Text("New? Get Started", style: TextStyle(color: Colors.white),),
                            ),
                          ),
                        ),
                      )

                    ],
                  ),

                ),
              )
            ],
          ),
        ),
      ),
    );

  }
}



