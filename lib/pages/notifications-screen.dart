
import 'package:dropdown_menu/dropdown_menu.dart';
import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
//import 'package:image_selector_formfield/image_selector_formfield.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:postmanug/util/CustomShapeClipper.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/widgets/app-bar.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/date-time-pickers.dart';
import 'package:postmanug/widgets/inline-double-display.dart';
import 'package:postmanug/widgets/text-input-field.dart';


class NotificationsScreen extends StatefulWidget{

  const NotificationsScreen({Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return NotificationsScreenState();
  }

}


enum AccountType { individual, business }

class NotificationsScreenState extends State<NotificationsScreen>{




  @override
  Future<void> initState()  {



    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: CustomAppBar(
        title_str: "Notifications",
        has_icon: true,
        custom_icon: null,
        onBackIconPressed: null,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,

          child: ListView.separated(
            separatorBuilder: (context, index) => Divider(
//              color: Colors.transparent,
            ),
            itemCount: 30,
            itemBuilder: (context, index) {
              return NotificationDetailsWidget();
            },


          )

        ),
      ),
    );
  }

}

class NotificationDetailsWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return NotificationDetailsWidgetState();
  }

}

class NotificationDetailsWidgetState extends State<NotificationDetailsWidget>{

  bool is_selected = false;


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(

      onLongPress: (){
        setState(() {
          is_selected=!is_selected;

        });
      },
      child: Padding(
        padding: EdgeInsets.all(5),
        child:Container(
          padding: EdgeInsets.all(16),


          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            color:Colors.transparent
          ),


          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[

          Container(
          padding: EdgeInsets.all(10),
            color: (is_selected?MyTheme.lightCorl:Colors.transparent),
            child:
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,

              children: <Widget>[
                Container(
                  width: (MediaQuery.of(context).size.width*0.73),
                  child:  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[

                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "1d",
                            style: TextStyles.airbnbCerealBook.copyWith(
                                color: MyTheme.shipGray,
                                fontSize: MyTheme.body4
                            ),
                          ),

                          Icon(Icons.keyboard_arrow_down,
                            size: 15,
                            color:MyTheme.shipGray ,)
                        ],
                      )
                      ,

                      Text(
                        "Some title",
                        style: TextStyles.airbnbCerealBook.copyWith(
                            color: Colors.black,
                            fontSize: MyTheme.heading4
                        ),
                      ),

                      Text(
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ",

                        softWrap: true,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,

                        style: TextStyles.airbnbCerealBook.copyWith(
                            color: MyTheme.shipGray,
                            fontSize: MyTheme.body4
                        ),
                        textAlign: TextAlign.left,
                      ),


                    ],
                  ),
                ),

                Image.asset(
                  "assets/images/app_icon.png",
                  width: 40,
                  height: 40,
                )
              ],
            ),

          )


              ,

              (is_selected ? Container(
                padding: EdgeInsets.all(10),
                color: MyTheme.light_grey,

                child: Row(
                  children: <Widget>[

                    InkWell(
                      child:
                      Text("ARCHIVE",
                        style: TextStyles.airbnbCerealBook.copyWith(
                            color: MyTheme.milanoRed,
                            fontSize: 15
                        ),
                      )
                      ,
                    ),

                    SizedBox(
                      width: 20,
                    ),



                    InkWell(
                      child:
                      Text("DELETE",
                        style: TextStyles.airbnbCerealBook.copyWith(
                            color: MyTheme.milanoRed,
                            fontSize: 15
                        ),
                      )
                      ,
                    )

                  ],

                )

                ,
              )
                  : Container()
              )




            ],
          ),
        ),
      ),
    );
  }

}
