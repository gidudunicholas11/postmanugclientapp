
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart' as words;
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/util/validator.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/text-input-field.dart';



class DestinationSearch extends StatefulWidget {
  var destination;
  DestinationSearch({Key key, this.destination}) : super(key: key);

  //PackageDetailsScreen({Key key, this.pkg_details, this.image_str}) : super(key: key);

  final String title = "Set Destination";

  @override
  _DestinationSearchState createState() => _DestinationSearchState();
}

class _DestinationSearchState extends State<DestinationSearch> {
//  final List<String> kWords;
  // _SearchAppBarDelegate _searchDelegate;

//  List addresses = List();
//  List hist = List();

  //Initializing with sorted list of english words
  /*_DestinationSearchState()
      : kWords = List.from(Set.from(words.all))
    ..sort(
          (w1, w2) => w1.toLowerCase().compareTo(w2.toLowerCase()),
    ),
        super();*/


  @override
  void initState() {
    super.initState();
    //Initializing search delegate with sorted list of English words
//
//    Util.getLastSearch().then((l) {
//      hist = l;
//    });


//    getAddresses();


  }

  final _setdestinationformKey = GlobalKey<FormState>();

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController mapcodecontroller = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: MyTheme.milanoRed,
        leading: IconButton(icon: Icon(Icons.arrow_back,), onPressed: () {
          Navigator.pop(context);
        },),
        automaticallyImplyLeading: false,
        title: Text('Set Destination'),

        actions: <Widget>[
          //Adding the search widget in AppBar
          IconButton(
            tooltip: 'Search',
            icon: const Icon(Icons.search),
            //Don't block the main thread
            onPressed: () {
              // showSearchPage(context, _searchDelegate);
            },
          ),
        ],
      ),

      body: Container(
        child: Column(
          children: <Widget>[
            Form(
              key: _setdestinationformKey,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),

                 child:
                 TextFormField(
                   controller: mapcodecontroller,
                   decoration: const InputDecoration(
                     hintText: 'Enter Map Code',
                     labelText: 'Map Code *',

                   ),
                   onSaved: (String value) {
                     // This optional block of code can be used to run
                     // code when the user saves the form.
                   },
                   validator: (String value) {
                     if(value.isEmpty){
                       return 'Map code is required';
                     }
                     return value.contains('@') ? 'Do not use the @ char.' : null;
                   },
                 )
//                child: MyTextInputField(
//                  hint: "Enter Map Code",
//                  show_label: true,
//                  Label: "Map Code",
//                  controller: mapcodecontroller,
//                  validator: (value) {
//                    return Validator().validateName(value);
//                  },
//
//                ),
              ),
            ),

          CustomButton2(
            title: "Set Destination",
           onPressed: () async {
                if(this._setdestinationformKey.currentState.validate()) {
                  String res = await HttpUtil.get(
                      "http://52.89.82.130:9015/api/v2/addresses/?map_code=" +
                          mapcodecontroller.text, with_auth: true
                  );

                  print(res);


                  if(res.contains('map_code')){

                    String jsonsDataString = res.toString();
                    final jsonData = jsonDecode(jsonsDataString);

                    print(jsonData);

                    Map a = new Map();

                    a['map_code']= jsonData[0]['map_code'];
                    a['address_id']= jsonData[0]['address_id'];
                    a['coordinates'] = jsonData[0]['coordinates'];
//                    a['height']=_controller_height.text;
//                    a['weight']=_controller_weight.text;
//                    a['additional_information']=_controller_additional.text;
//                    a['quantity']=_controller_quantity.text;
//                    a['name']=_controller_name.text;
//                    a['image']= image_base64;
                    Navigator.pop(context,json.encode(
                        a
                    ));

                   // print(jsonData[0]['map_code']);

                  }else if(int.parse(res.length.toString())  < 3){
                    _scaffoldKey.currentState.showSnackBar(
                        new SnackBar(
                          backgroundColor: MyTheme.milanoRed,
                          duration: new Duration(seconds: 5), content:
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[

                            new Text(    'Invalid map code'),

                          ],
                        ),
                        ));
                  }else{

                    _scaffoldKey.currentState.showSnackBar(
                        new SnackBar(
                          backgroundColor: MyTheme.milanoRed,
                          duration: new Duration(seconds: 5), content:
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[

                            new Text(    'Check your connection'),

                          ],
                        ),
                        ));

                  }
                }

           },
         //    onPressed: () async {
          )
          ],
        ),
      ),

    );
  }
}



//      body: Scrollbar(
//        //Displaying all English words in list in app's main page
//        child: ListView.builder(
//          itemCount: addresses.length,
////          itemCount: 0,
//          itemBuilder: (context, idx) =>
//
//             ListTile(
//        leading: Icon(Icons.location_on),
//      // Highlight the substring that matched the query.
//      title: Column(
//        crossAxisAlignment: CrossAxisAlignment.start,
//        children: <Widget>[
//          RichText(
//            text: TextSpan(
//              text: addresses[idx]['map_code'],
//              style: TextStyles.airbnbCerealMedium.copyWith(fontWeight: FontWeight.bold, color: Colors.black),
//
//            ),
//          ),
//
//          RichText(
//            text: TextSpan(
//              text: addresses[idx]['street']+", "+addresses[idx]['physical_address'],
//              style: TextStyles.airbnbCerealBook.copyWith(fontWeight: FontWeight.normal, fontStyle: FontStyle.italic, color: Colors.black),
//
//            ),
//          )
//        ],
//      ),
//      onTap: () {
//
//        Navigator.pop(context,json.encode(addresses[idx]));
////            onSelected(suggestion['map_code']);
//      },
//    )
//        ),
//      ),
//);
//}


//  Future<void> getAddresses() async {
//    String res = await HttpUtil.get(
//        URLS.addresses,with_auth: true
//    );
//
//    addresses= json.decode(res);
//    _searchDelegate = _SearchAppBarDelegate(addresses,hist);
//    print(res);
//
//    setState(() {
//
//    });
//  }

//Shows Search result
//  void showSearchPage(BuildContext context,
//      _SearchAppBarDelegate searchDelegate) async {
//    final String selected = await showSearch<String>(
//      context: context,
//      delegate: searchDelegate,
//    );
//
//    if (selected != null) {
//      Scaffold.of(context).showSnackBar(
//        SnackBar(
//          content: Text('selected: $selected'),
//        ),
//      );
//    }
//  }
//}
//
////Search delegate
//class _SearchAppBarDelegate extends SearchDelegate<String> {
//   List _words;
//   List _history;
//
//  _SearchAppBarDelegate(List words, List hist)
//      : _words = words,
//  //pre-populated history of words
//        _history = hist,
//        super();

// Setting leading icon for the search bar.
//Clicking on back arrow will take control to main page
//  @override
//  Widget buildLeading(BuildContext context) {
//    return IconButton(
//      tooltip: 'Back',
//      icon: AnimatedIcon(
//        icon: AnimatedIcons.menu_arrow,
//        progress: transitionAnimation,
//      ),
//      onPressed: () {
//        //Take control back to previous page
//        this.close(context, null);
//      },
//    );
//  }

// Builds page to populate search results.
//  @override
//  Widget buildResults(BuildContext context) {
//    return Padding(
//      padding: const EdgeInsets.all(8.0),
//      child: Center(
//        child: Column(
//          mainAxisSize: MainAxisSize.min,
//          children: <Widget>[
//            Text('===Chosen==='),
//            GestureDetector(
//              onTap: () {
//                //Define your action when clicking on result item.
//                //In this example, it simply closes the page
//                this.close(context, this.query);
//              },
//              child: Text(
//                this.query,
//                style: Theme.of(context)
//                    .textTheme
//                    .display2
//                    .copyWith(fontWeight: FontWeight.normal),
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
//  }

// Suggestions list while typing search query - this.query.
//  @override
//  Widget buildSuggestions(BuildContext context) {
//    final Iterable suggestions = this.query.isEmpty
//        ? _history
//        : _words.where((word) => true);
//    return _WordSuggestionList(
//      query: this.query,
//      suggestions: suggestions.toList(),
//      onSelected: (var suggestion) {
//        this.query = suggestion;
//        this._history.insert(0, suggestion);
//        showResults(context);
//      },
//    );
//  }

// Action buttons at the right of search bar.
//  @override
//  List<Widget> buildActions(BuildContext context) {
//    return <Widget>[
//      query.isNotEmpty ?
//      IconButton(
//        tooltip: 'Clear',
//        icon: const Icon(Icons.clear),
//        onPressed: () {
//          query = '';
//          showSuggestions(context);
//        },
//      ) : IconButton(
//        icon: const Icon(Icons.mic),
//        tooltip: 'Voice input',
//        onPressed: () {
//          this.query = 'TBW: Get input from voice';
//        },
//
//      ),
//    ];
//  }
//}

// Suggestions list widget displayed in the search page.
//class _WordSuggestionList extends StatelessWidget {
//  const _WordSuggestionList({this.suggestions, this.query, this.onSelected});
//
//  final List suggestions;
//  final String query;
//  final ValueChanged<dynamic> onSelected;
//
//  @override
//  Widget build(BuildContext context) {
//    final textTheme = Theme.of(context).textTheme.subhead;
//    return ListView.builder(
//      itemCount: suggestions.length,
//      itemBuilder: (BuildContext context, int i) {
//        var suggestion = suggestions[i];
//        return ListTile(
//          leading: query.isEmpty ? Icon(Icons.history) : Icon(Icons.location_on),
//          // Highlight the substring that matched the query.
//          title: Column(
//            crossAxisAlignment: CrossAxisAlignment.start,
////            mainAxisAlignment: MainAxisAlignment.start,
//            children: <Widget>[
//              RichText(
//                text: TextSpan(
//                  text: suggestion['map_code'].substring(0, query.length),
//                  style: textTheme.copyWith(fontWeight: FontWeight.bold),
//                  children: <TextSpan>[
//                    TextSpan(
//                      text: suggestion['map_code'].substring(query.length),
//                      style: textTheme,
//                    ),
//                  ],
//                ),
//              ),
//
//              RichText(
//                text: TextSpan(
//                  text: suggestion['street']+", "+suggestion['physical_address'],
//                  style: textTheme.copyWith(fontWeight: FontWeight.normal, fontStyle: FontStyle.italic),
//
//                ),
//              )
//            ],
//          ),
//          onTap: () {
//
//            Navigator.pop(context,json.encode(suggestion));
////            onSelected(suggestion['map_code']);
//          },
//        );
//      },
//    );
//  }
//
//
//
//
//
//}




