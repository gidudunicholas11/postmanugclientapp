

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:http/http.dart' as http;
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/pages/addresses-list.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/util/validator.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/text-input-field.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'package:enhanced_drop_down/enhanced_drop_down.dart';


import 'account-created.dart';
import 'home.dart';
// import 'package:social_login/social_login.dart';


class AddFirstAddress extends StatefulWidget{





  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new AddFirstAddressState();
  }

}

class AddFirstAddressState extends State<AddFirstAddress> {
  TextEditingController controller_hpuse_number;
  TextEditingController controller_street;
  TextEditingController controller_physical_address;

  Position _currentPosition;

  final _formKey = GlobalKey<FormState>();

  var image_file = null;
  var img_base64 = null;


  List zones_list = new List();
  List district_list = new List();
  List county_list = new List();
  List sub_county_list = new List();
  List parish_list = new List();

  String zone, district, county, sub_county, parish;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    controller_hpuse_number = new TextEditingController();
    controller_street = new TextEditingController();
    controller_physical_address = new TextEditingController();
    _currentPosition = Position(longitude: 0, latitude: 0);

//    check user exists

//  checkUser();
    _getCurrentLocation();

    getZones();
  }


  _getCurrentLocation() {
    final Geolocator geolocator = Geolocator();

    var locationOptions = LocationOptions(
        accuracy: LocationAccuracy.high, distanceFilter: 10);

    StreamSubscription<Position> positionStream = geolocator.getPositionStream(
        locationOptions).listen(
            (Position position) {
              print(position.longitude);
          _currentPosition = position;
          setState(() {

          });
        });
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Size size = MediaQuery
        .of(context)
        .size;


    return Scaffold(
//      backgroundColor: MyTheme.primaryColor,

      appBar: new AppBar(
        title: new Text("Generate Address Code"),
        centerTitle: false,
        elevation: 0.0,
        backgroundColor: MyTheme.primaryColor,


      ),


      body: SingleChildScrollView(
          child: Padding(padding: EdgeInsets.all(16),

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[

                /* SizedBox(
              height: 100,
            ),

            Center(
              child:
              Image.asset(
                "images/logo.png",
                height: 80,
                width: 80,
              ),
            ),*/

                /*SizedBox(
              height: 80,
            ),*/

                Center(
                  child: Column(
                    children: <Widget>[
                      Text(
                        "National Postcode",
                        textAlign: TextAlign.center,

                        style: TextStyles.airbnbCerealMedium.copyWith(
                          fontSize: 20,
                          color: MyTheme.black,

                        ),
                      ),

                      Text(
                        "Based on the location of your residence.",
                        textAlign: TextAlign.center,

                        style: TextStyles.airbnbCerealBook.copyWith(
                          fontSize: 16,
                          color: MyTheme.black,

                        ),
                      ),
                    ],
                  ),
                ),


                Form(
                    key: _formKey,
                    child: Column(
                        children: <Widget>[


                          MyTextInputField(
                            hint: "House Number",
                            show_label: true,
                            Label: "House Number",
                            controller: controller_hpuse_number,

                            validator: (value) {
                              return Validator().validateRequired(value);
                            },

                          ),

                          MyTextInputField(
                            hint: "Street",
                            show_label: true,
                            Label: "Street",
                            controller: controller_street,
                            validator: (value) {
                              return Validator().validateName(value);
                            },

                          ),
                          MyTextInputField(
                            hint: "Physical Address",
                            show_label: true,
                            Label: "Physical Address",
                            controller: controller_physical_address,
                            validator: (value) {
                              return Validator().validateName(value);
                            },

                          ),
                          SizedBox(height: 20,),

                          new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Expanded(
                                child: new Column(
                                  children: <Widget>[


                                    Text(
                                      "Zones",
                                      textAlign: TextAlign.left,
                                      style: TextStyles.airbnbCerealBook
                                          .copyWith(
                                          color: Colors.black,
                                          fontSize: MyTheme.heading4
                                      ),
                                    ),

                                    Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              15.0),
                                          border: Border.all(
                                              color: Colors.grey,
                                              style: BorderStyle.solid,
                                              width: 0.80),
                                        ),
                                        width: size.width * 0.45,
                                        child: DropdownButton<String>(

                                          value: zone,
                                          icon: Icon(Icons.arrow_downward),
                                          iconSize: 24,
                                          elevation: 16,
                                          style: TextStyle(
                                              color: Colors.black
                                          ),
                                          underline: Container(
                                            height: 2,
                                            color: Colors.transparent,
                                          ),
                                          onChanged: (String newValue) {
                                            var item = json.decode(newValue);
                                            zone = newValue;
                                            setState(() {


                                            });
//                                     print(item['districts']);

                                            getDistricts(item['districts']);
                                          },

                                          items: zones_list
                                              .map<DropdownMenuItem<String>>((
                                              var item) {
                                            return DropdownMenuItem<String>(
                                              value: json.encode(item),
                                              child: Text(item['name']),
                                            );
                                          })
                                              .toList(),
                                        )
                                    ),


                                  ],
                                ),
                              ),

                              Expanded(
                                child: new Column(
                                  children: <Widget>[


                                    Text(
                                      "Districts",
                                      textAlign: TextAlign.left,
                                      style: TextStyles.airbnbCerealBook
                                          .copyWith(
                                          color: Colors.black,
                                          fontSize: MyTheme.heading4
                                      ),
                                    ),

                                    Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              15.0),
                                          border: Border.all(
                                              color: Colors.grey,
                                              style: BorderStyle.solid,
                                              width: 0.80),
                                        ),
                                        width: size.width * 0.45,
                                        child: DropdownButton<String>(

                                          value: district,
                                          icon: Icon(Icons.arrow_downward),
                                          iconSize: 24,
                                          elevation: 16,
                                          style: TextStyle(
                                              color: Colors.black
                                          ),
                                          underline: Container(
                                            height: 2,
                                            color: Colors.transparent,
                                          ),
                                          onChanged: (String newValue) {
                                            var item = json.decode(newValue);
                                            district = newValue;
                                            setState(() {


                                            });


                                     if(item['name']=="KAMPALA"){
                                       getCounties(item['divisions']);
                                     }else{
                                       getCounties(item['counties']);
                                     }


                                          },

                                          items: district_list
                                              .map<DropdownMenuItem<String>>((
                                              var item) {
                                            return DropdownMenuItem<String>(
                                              value: json.encode(item),
                                              child: Text(item['name']),
                                            );
                                          })
                                              .toList(),
                                        )
                                    ),


                                  ],
                                ),
                              ),


                            ],
                          ),
                          SizedBox(height: 20,),
                          new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Expanded(
                                child: new Column(
                                  children: <Widget>[


                                    Text(
                                      "County",
                                      textAlign: TextAlign.left,
                                      style: TextStyles.airbnbCerealBook
                                          .copyWith(
                                          color: Colors.black,
                                          fontSize: MyTheme.heading4
                                      ),
                                    ),

                                    Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              15.0),
                                          border: Border.all(
                                              color: Colors.grey,
                                              style: BorderStyle.solid,
                                              width: 0.80),
                                        ),
                                        width: size.width * 0.45,
                                        child: DropdownButton<String>(

                                          value: county,
                                          icon: Icon(Icons.arrow_downward),
                                          iconSize: 24,
                                          elevation: 16,
                                          style: TextStyle(
                                              color: Colors.black
                                          ),
                                          underline: Container(
                                            height: 2,
                                            color: Colors.transparent,
                                          ),
                                          onChanged: (String newValue) {
                                            var item = json.decode(newValue);
                                            setState(() {
                                              county = newValue;
                                            });

//
                                            var dist = json.decode(zone);
                                            if(dist['name']=="KAMPALA"){
                                              parish = null;
                                              sub_county=null;
//                                              county=null;
                                              parish_list = new List();
                                              sub_county_list = new List();

                                              setState(() {

                                              });
                                              getParishes(
                                                  item['parishes']);
                                            }else{
                                              getSubCounties(
                                                  item['sub_counties']);
                                            }

                                     print(item);


                                          },

                                          items: county_list
                                              .map<DropdownMenuItem<String>>((
                                              var item) {
                                            return DropdownMenuItem<String>(
                                              value: json.encode(item),
                                              child: Text(item['name']),
                                            );
                                          })
                                              .toList(),
                                        )
                                    ),


                                  ],
                                ),
                              ),

                              Expanded(
                                child: new Column(
                                  children: <Widget>[


                                    Text(
                                      "Sub County",
                                      textAlign: TextAlign.left,
                                      style: TextStyles.airbnbCerealBook
                                          .copyWith(
                                          color: Colors.black,
                                          fontSize: MyTheme.heading4
                                      ),
                                    ),

                                    Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              15.0),
                                          border: Border.all(
                                              color: Colors.grey,
                                              style: BorderStyle.solid,
                                              width: 0.80),
                                        ),
                                        width: size.width * 0.45,
                                        child: DropdownButton<String>(

                                          value: sub_county,
                                          icon: Icon(Icons.arrow_downward),
                                          iconSize: 24,
                                          elevation: 16,
                                          style: TextStyle(
                                              color: Colors.black
                                          ),
                                          underline: Container(
                                            height: 2,
                                            color: Colors.transparent,
                                          ),
                                          onChanged: (String newValue) {
                                            var item = json.decode(newValue);
                                            setState(() {
                                              sub_county = newValue;
                                            });
//                                            print(item);

                                            getParishes(item['parishes']);
                                          },

                                          items: sub_county_list
                                              .map<DropdownMenuItem<String>>((
                                              var item) {
                                            return DropdownMenuItem<String>(
                                              value: json.encode(item),
                                              child: Text(item['name']),
                                            );
                                          })
                                              .toList(),
                                        )
                                    ),


                                  ],
                                ),
                              ),




                            ],
                          ),
                          SizedBox(height: 20,),

                          new Row(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Expanded(
                                child: new Column(
                                  children: <Widget>[


                                    Text(
                                      "Parishes",
                                      textAlign: TextAlign.left,
                                      style: TextStyles.airbnbCerealBook
                                          .copyWith(
                                          color: Colors.black,
                                          fontSize: MyTheme.heading4
                                      ),
                                    ),

                                    Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10.0),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              15.0),
                                          border: Border.all(
                                              color: Colors.grey,
                                              style: BorderStyle.solid,
                                              width: 0.80),
                                        ),
                                        width: size.width * 0.45,
                                        child: DropdownButton<String>(

                                          value: parish,
                                          icon: Icon(Icons.arrow_downward),
                                          iconSize: 24,
                                          elevation: 16,
                                          style: TextStyle(
                                              color: Colors.black
                                          ),
                                          underline: Container(
                                            height: 2,
                                            color: Colors.transparent,
                                          ),
                                          onChanged: (String newValue) {
                                            var item = json.decode(newValue);
                                            setState(() {
                                              parish = newValue;
                                            });
//                                        print(item);

//                                        getSubCounties(item['sub_counties']);
                                          },

                                          items: parish_list
                                              .map<DropdownMenuItem<String>>((
                                              var item) {
                                            return DropdownMenuItem<String>(
                                              value: json.encode(item),
                                              child: Text(item['name']),
                                            );
                                          })
                                              .toList(),
                                        )
                                    ),


                                  ],
                                ),
                              ),




                            ],
                          ),

                          SizedBox(height: 20,),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[

                              Text(
                                  "Lat: " + _currentPosition.latitude.toString()
                              ),

                              SizedBox(
                                width: 20,
                              ),

                              Text(
                                  "Long: " +
                                      _currentPosition.longitude.toString()
                              )

                            ],
                          ),

                          Center(
                            child: Text(
                              "These coordinates represent the location of your address",
                              textAlign: TextAlign.center,

                            ),
                          )


                        ]
                    )
                ),

                SizedBox(
                  height: 10,
                ),
                // A


                CustomButton2(
                  title: "Add Address",
                  onPressed: () async {
                    if(parish==null){
                      Util.basicAlert(context, 'Please make sure the parish is selected');
                    }else
                    if(_currentPosition.latitude==0 || _currentPosition.longitude==0){
                    Util.basicAlert(context, 'Location not yet picked');
                    _getCurrentLocation();
                  }else
                    if (_formKey.currentState.validate()) {





//                      check if parish is selected

//                    if(parish!=null){
                      String national_post_code = "10291";
                     /* var dist = json.decode(zone);
                      if(dist['name']=="KAMPALA"){
                       national_post_code =
                      }else{
                        getSubCounties(
                            item['sub_counties']);
                      }*/
                      var post_code = null;
                      if(parish!=null){
                        post_code = json.decode(parish)['post_code'];
                      }



                     print(URLS.clientprofile+(await Util.getPrefString(Util.CLIENT_ID) as String)+"/addresses/");
                      String res = await HttpUtil.post(
                          URLS.clientprofile+(await Util.getPrefString(Util.CLIENT_ID) as String)+"/addresses/",{
                    'coordinates':_currentPosition.latitude.toString()+","+_currentPosition.longitude.toString(),
                    'national_post_code':post_code,
                  'street':controller_street.text,
                  'house_number':controller_hpuse_number.text,
                  'physical_address':controller_physical_address.text,
                  }, with_auth: true
                  );






                  print(res);

                  var rp = json.decode(res);
                  if(rp.containsKey('url')) {


                    Util.saveAddItemToListPrefString(Util.MY_ADDRESSES, res);

                    Alert(
                      context: context,
                      type: AlertType.success,
                      title: "Notification",
                      desc: "Address Added Successfully\n Your map code is \n"+rp['map_code']+"\n\n"+"This is the code you shall use when sending and receiving packages. Please memorize it",
                      buttons: [
                        DialogButton(
                          child: Text(
                            "Okay",
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                          onPressed: () async {
                            Navigator.pop(context);
                            Navigator.pop(context);
                            var user = json.decode(await Util.getPrefString(Util.LAST_LOGIN_USER));
                            Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) =>
                                    ProfilePreviewScreen(
                                      item: user,
                                      address: rp,

                                    )
                                )
                            );
                          },
                          width: 120,
                        )
                      ],
                    ).show();
                  }else{
                    Alert(
                      context: context,
                      type: AlertType.error,
                      title: "Notification",
                      desc: "Error adding an address",
                      /*buttons: [
                        DialogButton(
                          child: Text(
                            "Okay",
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) =>
                                    HomePage(

                                    )
                                )
                            );
                          },
                          width: 120,
                        )
                      ],*/
                    ).show();
                  }



//                    }else{
//
//                      Alert(
//                        context: context,
//                        type: AlertType.error,
//                        title: "Notification",
//                        desc: "Please enter all country code information",
//                        /*buttons: [
//                          DialogButton(
//                            child: Text(
//                              "Okay",
//                              style: TextStyle(color: Colors.white, fontSize: 20),
//                            ),
//                            onPressed: (){
//                              *//*Navigator.pop(context);
//                              Navigator.push(
//                                  context,
//                                  MaterialPageRoute(builder: (context) =>
//                                      HomePage(
//
//                                      )
//                                  )
//                              );*//*
//                            },
//                            width: 120,
//                          )
//                        ],*/
//                      ).show();
//                    }


                      // If the form is valid, display a snackbar. In the real world,
                      // you'd often call a server or save the information in a database.
//                  print(controller_email.text);

//                  Util.snack(context, "User Accounted updated");


//
//
//                  processLogin(res, context);

                    }else{
                    }
                  },
                ),
                SizedBox(
                  height: 10,
                ),


              ],
            )
            ,)
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Future<void> getZones() async {
    String res = await HttpUtil.get(
        URLS.zones, with_auth: true
    );

    county = null;
    district = null;
    sub_county=null;
    district_list=List();
    sub_county_list=List();
    parish=null;
    parish_list=List();
    county_list = new List();
    setState(() {

    });

    zones_list = json.decode(res);

    setState(() {

    });
  }

  Future<void> getDistricts(String url) async {
    print(url);

    String res = await HttpUtil.get(
        url, with_auth: true
    );

    county = null;
    district = null;
    sub_county=null;
    sub_county_list=List();
    parish=null;
    parish_list=List();
    county_list = new List();

    district_list = json.decode(res);
    setState(() {

    });
  }

  Future<void> getCounties(var url) async {
    String res = await HttpUtil.get(
        url, with_auth: true
    );

    sub_county = null;
    parish=null;
    parish_list=null;
    sub_county_list = new List();
    parish_list = List();

    county_list = json.decode(res);
    setState(() {

    });
  }

  Future<void> getSubCounties(var url) async {
    String res = await HttpUtil.get(
        url, with_auth: true
    );

    parish = null;
    sub_county=null;
    parish_list = new List();

    sub_county_list = json.decode(res);
    setState(() {

    });
  }

  Future<void> getParishes(var url) async {
    String res = await HttpUtil.get(
        url, with_auth: true
    );

    print(url);

    print(res);
    parish=null;
    parish_list = json.decode(res);

    setState(() {

    });
  }

}



