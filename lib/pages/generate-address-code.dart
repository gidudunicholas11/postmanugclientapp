

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:http/http.dart' as http;
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/pages/add-address.dart';
import 'package:postmanug/pages/addresses-list.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/util/validator.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/text-input-field.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'package:enhanced_drop_down/enhanced_drop_down.dart';


import 'add-first-address.dart';
import 'home.dart';
// import 'package:social_login/social_login.dart';


class GenerateAddressCode extends StatefulWidget{

var item;

   GenerateAddressCode({Key key, this.item}) : super(key: key);




  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new GenerateAddressCodeState();
  }

}

class GenerateAddressCodeState extends State<GenerateAddressCode> {


  final _formKey = GlobalKey<FormState>();



  @override
  void initState() {
    // TODO: implement initState
    super.initState();


  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Size size = MediaQuery
        .of(context)
        .size;


    return Scaffold(
//      backgroundColor: MyTheme.primaryColor,

      appBar: new AppBar(
        title: new Text("Generate Address Code"),
        centerTitle: false,
        elevation: 0.0,
        backgroundColor: MyTheme.primaryColor,


      ),


      body: SingleChildScrollView(
          child: Padding(padding: EdgeInsets.all(16),

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                
                Center(
                  child:Text("Generate Address code",
                  style: TextStyles.airbnbCerealMedium.copyWith(
                    fontSize: 18,
                    color: Colors.black
                  ),
                  ),
                ),

                Center(
                  child:Text("Select start to generate your address code.",
                    style: TextStyles.airbnbCerealBook.copyWith(
                        fontSize: 16,
                        color: Colors.black
                    ),
                  ),
                ),
                SizedBox(height: 30,),

                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new OutlineButton(
                        child: new Text("DO LATER"),
                        onPressed: null,
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0))
                    ),

                    SizedBox(width: 50,),

                    new OutlineButton(
                        child: Text("START",
                          style: TextStyles.airbnbCerealMedium.copyWith(
                              fontSize: 18,
                              color: Colors.black
                          ),
                        ),
                        onPressed: (){
                          Navigator.pop(context);
                          Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AddFirstAddress(
                          )));

                        },
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0))
                    )
                  ],
                )








              ],
            )
            ,)
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }


}



