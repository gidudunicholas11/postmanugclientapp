import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/provider.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_map_polyline/google_map_polyline.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/entity/location_entity.dart';
import 'package:postmanug/pages/account.dart';
import 'package:postmanug/pages/addresses-list.dart';
import 'package:postmanug/pages/get-destination-address.dart';
import 'package:postmanug/pages/mail-history.dart';
import 'package:postmanug/pages/my-questions-page.dart';
import 'package:postmanug/pages/notifications-screen.dart';
import 'package:postmanug/pages/package-details.dart';
import 'package:postmanug/pages/settings.dart';
import 'package:postmanug/util/CustomShapeClipper.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../database.dart';
import '../main.dart';
import 'other_page.dart';

typedef OnLoationsChnaged(var origin, var destination);

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Location _locationService = new Location();
  String mainProfilePicture =
      "https://randomuser.me/api/portraits/women/44.jpg";
  String otherProfilePicture =
      "https://randomuser.me/api/portraits/women/47.jpg";
  Map user_item = {
    'profile': {
      'user': {'username': "", 'first_name': "", 'last_name': "", 'email': ""},
      'date_of_birth': "",
      'gender': "",
      'phone_number': "",
      'photo': 'http://54.186.202.6:9015/media/photos/clients/no-img.jpg',
      'client_uuid': '',
      'created_at': '',
      'post_office_box': '',
      'physical_address': '',
      'device_id': '',
      'client_id': '',
      'default_pin_activated': '',
      'account_type': ''
    },
    'token': ''
  };

  List my_addresses = List();

  String names;

  Completer<GoogleMapController> _controller = Completer();

  GoogleMapPolyline googleMapPolyline =
      new GoogleMapPolyline(apiKey: "AIzaSyCYeQIRZ2nK_5xXNdgZjYkl8bXUpZlcB70");

  GoogleMapController mapController;

  Map<PolylineId, Polyline> _polylines = <PolylineId, Polyline>{};
  int _polylineCount = 1;

  List<List<PatternItem>> patterns = <List<PatternItem>>[
    <PatternItem>[], //line
    <PatternItem>[PatternItem.dash(30.0), PatternItem.gap(20.0)], //dash
    <PatternItem>[PatternItem.dot, PatternItem.gap(10.0)], //dot
    <PatternItem>[
      //dash-dot
      PatternItem.dash(30.0),
      PatternItem.gap(20.0),
      PatternItem.dot,
      PatternItem.gap(20.0)
    ],
  ];

  Set<Marker> _markers = Set<Marker>();

  Position _currentPosition;
  var pickup_address;
  var destination_address;

  @override
  void initState() {
    // TODO: implement initState

    getUser();

    _currentPosition = Position(longitude: 0, latitude: 0);

    _getCurrentLocation().then((_) {
      setState(() {});
    });

    super.initState();
    _registerListener();
  }

  static void backgroundCallback(List<LocationData> locations) async {
//    print('Sample app received data location: $locations');

    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
        new FlutterLocalNotificationsPlugin();
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings);

    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'location_update',
        'Location Updates',
        'You will receive location updates here',
        importance: Importance.Max,
        priority: Priority.High);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
//    await flutterLocalNotificationsPlugin.show(
//        0, 'New Location Received !', "${locations[0].latitude}, ${locations[0].longitude}", platformChannelSpecifics);

    final database =
        await $FloorAppDatabase.databaseBuilder(Util.DB_NAME).build();

    var locs = await database.locationEntityDao.findAllLocations();
//    print(locs.length);
    if (locs.length > 0) {
      LocationEntity loc = locs.elementAt(0);
      loc.latitude = locations[0].latitude;
      loc.longitude = locations[0].longitude;
    } else {
      LocationEntity loc =
          new LocationEntity(1, locations[0].latitude, locations[0].longitude);
      await database.locationEntityDao.insertLocation(loc);
    }

//    print("${locations[0].latitude}, ${locations[0].longitude}");

//  change ma position
//  changeMapPosition(locations[0].latitude, locations[0].longitude);
  }

  void changeMapPosition(double latitude, double longitude) {
    CameraUpdate u2 =
        CameraUpdate.newLatLngZoom(new LatLng(latitude, longitude), 11);

    if (mapController != null) {
      mapController.animateCamera(u2);
    }
    print(mapController != null);
  }

  //Dialog

  //End of Dialog

  void _registerListener() async {
    bool _permission = await _locationService.requestPermission();
    print("Permission: $_permission");
    if (_permission) {
      bool statusBackgroundLocation =
          await _locationService.registerBackgroundLocation(backgroundCallback);
      print("statusBackgroundLocation: $statusBackgroundLocation");
    } else {
      print("Permission denied");
    }
  }

  void _removeListener() async {
    await _locationService.removeBackgroundLocation();
  }

  Future<void> _getCurrentLocation() async {
    /*final Geolocator geolocator = Geolocator();

    Position position = await Geolocator().getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
    setState(() {
      _currentPosition = position;
    });*/

    final database =
        await $FloorAppDatabase.databaseBuilder(Util.DB_NAME).build();

    var locs = await database.locationEntityDao.findAllLocations();
//    print(locs.length);
    if (locs.length > 0) {
      LocationEntity loc = locs.elementAt(0);

      _currentPosition =
          new Position(latitude: loc.latitude, longitude: loc.longitude);

      setState(() {});
    } else {}
  }

  Future<void> locationsChanged(var orig, var destination) async {
//    var origin = json.decode(orig);
//  print("something");

    if (orig != null && destination != null) {
      LatLng ori = LatLng(
          double.parse(orig['coordinates'].toString().split(",")[0]),
          double.parse(orig['coordinates'].toString().split(",")[1]));
      LatLng dest = LatLng(
          double.parse(destination['coordinates'].toString().split(",")[0]),
          double.parse(destination['coordinates'].toString().split(",")[1]));

      // LatLng ori = LatLng(0.3293,32.5711);
      // LatLng dest = LatLng(0.3482,32.5649);

      _markers.clear();

      LatLng temp;

      if (ori.latitude > dest.latitude) {
        temp = ori;
        ori = dest;
        dest = temp;
      }

      LatLngBounds bound = LatLngBounds(southwest: ori, northeast: dest);

      CameraUpdate u2 = CameraUpdate.newLatLngBounds(bound, 10);
      mapController.animateCamera(u2);

      /*if(true){
      return;
    }*/

      var res = await googleMapPolyline.getCoordinatesWithLocation(
          origin: ori, destination: dest, mode: RouteMode.driving);

//    print (res);

      setState(() {
        _polylines.clear();
      });
      _addPolyline(res);

      BitmapDescriptor sourceIcon = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(devicePixelRatio: 2.5), 'assets/images/pin.png');
      _markers.add(Marker(
        markerId: MarkerId("sourcePin"),
        position: LatLng(
            double.parse(orig['coordinates'].toString().split(",")[0]),
            double.parse(orig['coordinates']
                .toString()
                .split(",")[1])), // updated position
        icon: sourceIcon,
      ));

      BitmapDescriptor destIcon = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(devicePixelRatio: 2.5), 'assets/images/pin1.png');
      _markers.add(Marker(
        markerId: MarkerId("destPin"),
        position: LatLng(
            double.parse(destination['coordinates'].toString().split(",")[0]),
            double.parse(destination['coordinates']
                .toString()
                .split(",")[1])), // updated position
        icon: destIcon,
      ));

      setState(() {});
    }
  }

  void check(CameraUpdate u, GoogleMapController c) async {
    c.animateCamera(u);
    mapController.animateCamera(u);
    LatLngBounds l1 = await c.getVisibleRegion();
    LatLngBounds l2 = await c.getVisibleRegion();
    print(l1.toString());
    print(l2.toString());

    /*if (l1.southwest.latitude == -90 || l2.southwest.latitude == -90)
      check(u, c);
    else {
//      await setPolylines();
    }*/
  }

  _addPolyline(List<LatLng> _coordinates) {
    PolylineId id = PolylineId("poly$_polylineCount");
    Polyline polyline = Polyline(
        polylineId: id,
        patterns: patterns[0],
        color: MyTheme.primaryColor,
        points: _coordinates,
        width: 5,
        onTap: () {});

    setState(() {
      _polylines[id] = polyline;
      _polylineCount++;
    });
  }

  void switchUser() {
    String backupString = mainProfilePicture;
    this.setState(() {
      mainProfilePicture = otherProfilePicture;
      otherProfilePicture = backupString;
    });
  }

  Future<void> getUser() async {
    var p = json.decode(await Util.getPrefString(Util.LAST_LOGIN_USER));
    var params = p;

    user_item = params;

//    print(params);

    my_addresses = await Util.getPrefList(Util.MY_ADDRESSES);
    if (my_addresses == null) {
      my_addresses = List();
    }
    setState(() {});
  }

  final _scaffoldKeys = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKeys,
      appBar: new AppBar(
        title: Center(
          child: new Text("Post Man"),
        ),
        backgroundColor: MyTheme.milanoRed,
      ),
      drawer: new Drawer(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: new UserAccountsDrawerHeader(
                  accountEmail:
                      new Text(user_item['profile']['user']['username']),
                  accountName: new Text(user_item['profile']['user']
                          ['first_name'] +
                      " " +
                      user_item['profile']['user']['last_name']),
                  currentAccountPicture: new GestureDetector(
                    onTap: () => switchUser(),
                    child: new CircleAvatar(
                      backgroundImage: AdvancedNetworkImage(
                        user_item['profile']['photo'],
//                            header: header,
                        useDiskCache: true,
                        cacheRule: CacheRule(maxAge: const Duration(days: 7)),
                      ),
                    ),
                  ),
                  /*otherAccountsPictures: <Widget>[
                    new GestureDetector(
                      onTap: () => print("this is the other user"),
                      child: new CircleAvatar(
                          backgroundImage: new NetworkImage(otherProfilePicture)
                      ),
                    ),
                  ],*/
                  decoration: new BoxDecoration(color: MyTheme.milanoRed)),
            ),
            Flexible(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    new ListTile(
                        title: new Text("Profile"),
                        leading: new Icon(
                          Icons.person,
                          color: Colors.black,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new ProfileScreen(
                                    item: user_item,
                                    address: (my_addresses.length > 0)
                                        ? my_addresses.first
                                        : null,
                                  )));
                        }),
                    new ListTile(
                        title: new Text("Notifications"),
                        leading: new Icon(
                          Icons.notifications,
                          color: Colors.black,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new NotificationsScreen()));
                        }),
                    new ListTile(
                        title: new Text("Mail History"),
                        leading: new Icon(
                          Icons.mail,
                          color: Colors.black,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new MailHistory()));
                        }),
                    new ListTile(
                        title: new Text("Addresses"),
                        leading: new Icon(
                          Icons.settings,
                          color: Colors.black,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  AddressesList()));
                        }),
                    new ListTile(
                        title: new Text("Settings"),
                        leading: new Icon(
                          Icons.settings,
                          color: Colors.black,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  SettingsScreen()));
                        }),
                    new Divider(),
                    new ListTile(
                        title: new Text("FAQs"),
                        leading: new Icon(
                          Icons.info_outline,
                          color: Colors.black,
                        ),
                        onTap: () {
                          /* Navigator.of(context).pop();
                    Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new OtherPage("Second Page")));*/
                        }),
                    new ListTile(
                        title: new Text("Help and Feed Back"),
                        leading: new Icon(
                          Icons.question_answer,
                          color: Colors.black,
                        ),
                        onTap: () {
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new MyQuestionsScreen()));
                        }),
                    new ListTile(
                        title: new Text("Logout"),
//                  leading: new Icon(Icons.lgou, color: Colors.black,),
                        onTap: () {
                          Util.savePrefString(Util.USER_LOGGED_IN, "false");
                          Navigator.of(context).pop();
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new MyHomePage()));
                        })
                  ],
                ),
              ),
            )
          ],
        ),
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            markers: _markers,
            mapType: MapType.normal,
            initialCameraPosition: CameraPosition(
              target:
                  LatLng(_currentPosition.latitude, _currentPosition.longitude),
              zoom: 14.4746,
            ),
            myLocationButtonEnabled: true,
            myLocationEnabled: true,
            onMapCreated: (GoogleMapController controller) async {
              mapController = controller;
              _controller.complete(controller);

              final database =
                  await $FloorAppDatabase.databaseBuilder(Util.DB_NAME).build();

              var locs = await database.locationEntityDao.findAllLocations();
//    print(locs.length);
              if (locs.length > 0) {
                LocationEntity loc = locs.elementAt(0);
                changeMapPosition(loc.latitude, loc.longitude);
              } else {}

//    print("${locations[0].latitude}, ${locations[0].longitude}");

//  change ma position
            },
            polylines: Set<Polyline>.of(_polylines.values),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: LocationsPickupWidget(
              onLoationsChnaged: (var a, var b) {
                pickup_address = a;
                destination_address = b;
                locationsChanged(a, b);
              },
            ),
          )
        ],
      ),
    );
  }

  /*Future<void> _goToTheLake() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }*/
}

class LocationsPickupWidget extends StatefulWidget {
  OnLoationsChnaged onLoationsChnaged;

  LocationsPickupWidget({Key key, this.onLoationsChnaged}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LocationsPickupWidgetState();
  }
}

class LocationsPickupWidgetState extends State<LocationsPickupWidget> {
  List dropdownValues = List();

  String value;
//  String value1 = dropdownValues[0];
  var destination = null;
  var pkg_details = null;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getAddresses();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    // TODO: implement build
    return Column(
      children: <Widget>[
        Flexible(
//      height: height*0.35,
//      height: double.infinity,
//      width: double.infinity,
//      color: Colors.transparent,
            child: Align(
          alignment: Alignment.bottomCenter,
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: height * 0.25,
                  width: MediaQuery.of(context).size.width,
                  decoration: new BoxDecoration(
                      color: MyTheme.light_grey,
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(40.0),
                          topRight: const Radius.circular(40.0))),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                          left: width * 0.15, right: width * 0.15),
                      child: Container(
                        padding: EdgeInsets.all(20),
                        decoration: new BoxDecoration(
                            color: Colors.white,
                            borderRadius: new BorderRadius.all(
                                const Radius.circular(30.0))),
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Pickup location",
                                  textAlign: TextAlign.center,
                                ),
                                Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15.0),
                                      border: Border.all(
                                          color: Colors.black,
                                          style: BorderStyle.solid,
                                          width: 0.80),
                                    ),
                                    child: DropdownButton<String>(
                                      value: value,
                                      icon: Icon(Icons.arrow_downward),
                                      iconSize: 24,
                                      elevation: 16,
                                      style: TextStyle(color: Colors.black),
                                      underline: Container(
                                        height: 2,
                                        color: Colors.transparent,
                                      ),
                                      onChanged: (String newValue) {
                                        var item = json.decode(newValue);
                                        value = newValue;
                                        setState(() {});

                                        if (widget.onLoationsChnaged != null) {
                                          widget.onLoationsChnaged(
                                              json.decode(value), destination);
                                        }
                                      },
                                      items: dropdownValues
                                          .map<DropdownMenuItem<String>>(
                                              (var item) {
                                        return DropdownMenuItem<String>(
                                          value: json.encode(item),
                                          child: Text(item['map_code']),
                                        );
                                      }).toList(),
                                    )

                                    /* DropdownButton(
                                  items: dropdownValues
                                      .map((value) => DropdownMenuItem(
                                    child: Text(value),
                                    value: value,
                                  ))
                                      .toList(),
                                  onChanged: (String v) {

                                    setState(() {
                                      value= v;
                                    });
                                  },
                                  isExpanded: false,
                                  value: value,
                                ),*/
                                    )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  "Input Destination",
                                  textAlign: TextAlign.center,
                                ),
                                Container(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 2.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15.0),
                                    border: Border.all(
                                        color: Colors.black,
                                        style: BorderStyle.solid,
                                        width: 0.80),
                                  ),
                                  child: Container(
                                    width: 65,
                                    child: OutlineButton(
                                      onPressed: () async {
                                        String message = await Navigator.push(
                                            context,
                                            new MaterialPageRoute(
                                                builder: (context) =>
                                                    new DestinationSearch(
                                                      destination: destination,
                                                    )));
                                        destination = json.decode(message);
                                        setState(() {});

                                        if (widget.onLoationsChnaged != null) {
                                          widget.onLoationsChnaged(
                                              json.decode(value), destination);
                                        }
                                      },
                                      child: Center(
                                        child: new Text(
                                          (destination != null)
                                              ? destination['map_code']
                                              : "",
                                          style: new TextStyle(
                                              color: Colors.black,
                                              fontSize: 11,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: CustomButton(
                        title: (pkg_details == null
                            ? "Add Package Details"
                            : "Edit Package Details"),
                        onPressed: () async {
                          String message = await Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) =>
                                      new PackageDetailsScreen(
                                          pkg_details: pkg_details,
                                          origin:
                                              json.decode(value)['coordinates'],
                                          destination:
                                              destination['address_id'])));

                          pkg_details = json.decode(message);
//                              Navigator.push(
//                                  context,
//                                  MaterialPageRoute(builder: (context) => PackageDetailsScreen()));
                        },
                      ),
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        textColor: Colors.white,
                        color: MyTheme.shipGray,
                        child: Padding(
                          padding: EdgeInsets.all(20),
                          child: Text(
                            "CONFIRM",
                            style: TextStyles.airbnbCerealBook
                                .copyWith(color: Colors.white, fontSize: 17),
                          ),
                        ),
                        onPressed: () async {
                          String packageid = pkg_details['package_id'];
                          String res = await HttpUtil.post(
                              URLS.clientprofile +
                                  (await Util.getPrefString(Util.CLIENT_ID)
                                      as String) +
                                  "/packages/" +
                                  packageid +
                                  "/initiate_dispatch/",
                              {
                                'origin_coordinates':
                                    json.decode(value)['coordinates'],
                                'package_weight': pkg_details['weight'],
                                'package_dimensions': '9x3x2',
                                'package_name': pkg_details['name'],
                                'package_description':
                                    pkg_details['additional_information'],
                                'package_picture': pkg_details['image'],
//                              'package_picture':null,
                                'quantity': pkg_details['quantity'],
                                'destination_address':
                                    destination['address_id'],
                                'weight_unit': '6417885661295153642',
                                'dimensions_unit': '7107823495418089962',
                              },
                              with_auth: true);

                          print(res);

                          if (res.contains('Package dispatch initiated')) {
                            Fluttertoast.showToast(
                              backgroundColor: Colors.redAccent,
                              msg: 'Package Added Successfully',
                              timeInSecForIosWeb: 10,
                              toastLength: Toast.LENGTH_LONG,
                            );
                          } else if (res.contains(
                              ' on package not at created state denied')) {
                            Fluttertoast.showToast(
                              backgroundColor: Colors.redAccent,
                              msg: 'Package Already Exists',
                              timeInSecForIosWeb: 10,
                              toastLength: Toast.LENGTH_LONG,
                            );
                          }
                        },

//                              if(value!=null && destination!=null){
//                                if(pkg_details!=null){
//
//                                  String res = await HttpUtil.post(
//                                      URLS.clientprofile+(await Util.getPrefString(Util.CLIENT_ID) as String)+"/packages/",{
//                              'origin_coordinates':json.decode(value)['coordinates'],
//                              'package_weight':pkg_details['weight'],
//                              'package_dimensions': '9x3x2',
//                              'package_name':pkg_details['name'],
//                              'package_description':pkg_details['additional_information'],
//                              'package_picture':pkg_details['image'],
////                              'package_picture':null,
//                              'quantity':pkg_details['quantity'],
//                              'destination_address':destination['address_id'],
//                              'weight_unit': '6417885661295153642',
//                              'dimensions_unit': '7107823495418089962',
//                              }, with_auth: true
//                              );
////                              Util.saveEditItemInListPrefString(Util.MY_ADDRESSES, json.decode(res),"address_id");
//
//
//                              print(res);
//
//                             /* var rp = json.decode(res);
//                              if(rp.containsKey('url')) {
//                              Alert(
//                              context: context,
//                              type: AlertType.success,
//                              title: "Notification",
//                              desc: "Address Edit Successfully\n Your map code is \n"+rp['map_code'],
//                              buttons: [
//                              DialogButton(
//                              child: Text(
//                              "Okay",
//                              style: TextStyle(color: Colors.white, fontSize: 20),
//                              ),
//                              onPressed: () {
//                              Navigator.pop(context);
//                              Navigator.pop(context);
//                              Navigator.push(
//                              context,
//                              MaterialPageRoute(builder: (context) =>
//                              AddressesList(
//
//                              )
//                              )
//                              );
//                              },
//                              width: 120,
//                              )
//                              ],
//                              ).show();
//                              }else{
//                              Alert(
//                              context: context,
//                              type: AlertType.error,
//                              title: "Notification",
//                              desc: "Error adding an address",
//
//                              ).show();
//                              }*/
//
//
//
//                              }else{
//                                  Util.basicAlert(context, 'Please add the package details');
//                                }
//                              }else{
//                                Util.basicAlert(context, 'Please select the pickup and destination addresses');
//                              }

                        /*shape: RoundedRectangleBorder(
                     borderRadius: new BorderRadius.circular(18.0),
                     side: BorderSide(color: MyTheme.milanoRed),
                   ),*/
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ))
      ],
    );
  }

  Future<void> getAddresses() async {
    String res = await HttpUtil.get(
        URLS.clientprofile +
            (await Util.getPrefString(Util.CLIENT_ID) as String) +
            "/addresses/",
        with_auth: true);

    value = null;

    dropdownValues = json.decode(res);

    setState(() {});
  }

  void showFancyCustomDialog(BuildContext context) {
    Dialog fancyDialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
        ),
        height: 300.0,
        width: 300.0,
        child: Stack(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 300,
              decoration: BoxDecoration(
                color: Colors.grey[100],
                borderRadius: BorderRadius.circular(12.0),
              ),
            ),
            Container(
              width: double.infinity,
              height: 50,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Colors.greenAccent,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(12),
                  topRight: Radius.circular(12),
                ),
              ),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  "Fancy Dialog Title!",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: double.infinity,
                  height: 50,
                  decoration: BoxDecoration(
                    color: Colors.blue[300],
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(12),
                      bottomRight: Radius.circular(12),
                    ),
                  ),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Okay let's go!",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ),
            ),
            Align(
              // These values are based on trial & error method
              alignment: Alignment(1.05, -1.05),
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Icon(
                    Icons.close,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
    showDialog(
        context: context, builder: (BuildContext context) => fancyDialog);
  }
}

/**********************************************
 ***************Important Notes*****************
 **********************************************/

//onTap: () => print("this is the other user")
// use this to display log messages on tap
