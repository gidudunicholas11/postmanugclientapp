import 'package:flutter/material.dart';
import 'package:postmanug/constants/constants.dart';
import 'package:postmanug/util/http_util.dart';
import 'package:postmanug/util/prefs.dart';
import 'package:postmanug/util/theme.dart';
import 'package:postmanug/util/validator.dart';
import 'package:postmanug/widgets/custom-button.dart';
import 'package:postmanug/widgets/text-input-field.dart';

//class Edit extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    return MaterialApp(
//      theme: ThemeData(
//        primaryColor: MyTheme.milanoRed
//      ),
//      debugShowCheckedModeBanner: false,
//      home: MyEdit(),
//    );
//  }
//}

class MyEdit extends StatefulWidget {

  String phone,fname,lname,district,postcode,addresscode,country;
  //var address = null;


  MyEdit({Key key,this.phone,this.fname, this.lname, this.district, this.postcode, this.addresscode,this.country }) : super(key: key);
  @override
  _MyEditState createState() => _MyEditState();
}


class _MyEditState extends State<MyEdit> {
  final _editformKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    TextEditingController _controller_phone = new TextEditingController(text: widget.phone);
    TextEditingController _controller_fname = new TextEditingController(text: widget.fname);
    TextEditingController _controller_name = new TextEditingController(text: widget.lname);
    TextEditingController _controller_district = new TextEditingController(text: widget.district);
    TextEditingController _controller_national = new TextEditingController(text: widget.postcode);
    TextEditingController _controller_code = new TextEditingController(text:  widget.addresscode);
    TextEditingController _controller_country = new TextEditingController(text: widget.country);
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: MyTheme.milanoRed,
        leading: Icon(Icons.arrow_back),
        title: Text('Edit Profile'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 15.0),
          child: Form(
            key: _editformKey,
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Flexible(
                      child: MyTextInputField(
                        show_label: true,
                        Label: "Phone",

                        controller: _controller_phone,
                        validator: (value){
                          return Validator().validateRequired(value);
                        },

                      ),
                    ),
                    Flexible(
                      child: MyTextInputField(
                        show_label: true,
                        Label: "District",

                        controller: _controller_district,
                        validator: (value){
                          return Validator().validateRequired(value);
                        },

                      ),
                    ),


                  ],
                ),

                Row(
                  children: <Widget>[
                    Flexible(
                      child: MyTextInputField(
                        show_label: true,
                        Label: "Firstname",
                        controller: _controller_fname,
                        validator: (value){
                          return Validator().validateRequired(value);
                        },

                      ),
                    ),
                    Flexible(
                      child: MyTextInputField(
                        show_label: true,
                        Label: "Lastname",

                        controller: _controller_name,
                        validator: (value){
                          return Validator().validateRequired(value);
                        },

                      ),
                    ),


                  ],
                ),
                Row(
                  children: <Widget>[
                    Flexible(
                      child: MyTextInputField(
                        //hint: "Enter your first name",
                        show_label: true,
                        Label: "Address Code",

                        controller: _controller_code,
                        validator: (value){
                          return Validator().validateRequired(value);
                        },

                      ),
                    ),
                    Flexible(
                      child: MyTextInputField(
                        //hint: "Enter your first name",
                        show_label: true,
                        Label: "Postcode",

                        controller: _controller_national,
                        validator: (value){
                          return Validator().validateRequired(value);
                        },

                      ),
                    ),


                  ],
                ),

                MyTextInputField(
                  //hint: "Enter your first name",
                  show_label: true,
                  Label: "Country",

                  controller: _controller_country,
                  validator: (value){
                    return Validator().validateRequired(value);
                  },

                ),

               SizedBox(height: 20.0,),
                CustomButton(
                  title: "UPDATE",
                  onPressed: () async {

                    if(_editformKey.currentState.validate()){
                      var body = {
                        'user_first_name':_controller_fname.text,
                        'user_last_name':_controller_name.text,
                      };
                      
                      String response = await HttpUtil.patch(
                          URLS.clientprofile+(await Util.getPrefString(Util.CLIENT_ID) as String)+"/",body, with_auth: true
                    );

                      print('ok response is'+ response);

                    }else{

                    }
                    
                  },
                )


              ],
            ),
          ),
        ),
      ),
    );
  }
}
