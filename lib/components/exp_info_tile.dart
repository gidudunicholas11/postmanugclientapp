import 'dart:async';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:postmanug/util/common.dart';
import 'package:postmanug/util/theme.dart';

class ExpandedInfoTile extends StatelessWidget{
  final String title;
  final List<TileInfo> tile_infos_list;


  const ExpandedInfoTile({@required this.title, @required this.tile_infos_list});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return ExpandablePanel(
      header: Text(title,
        style: TextStyles.airbnbCerealMedium.copyWith(color: Colors.black,fontSize:  MyTheme.heading3),
      ),
      collapsed:  buildColapsedTile(tile_infos_list),

      expanded: buildExpandedTiles(tile_infos_list),
      tapHeaderToExpand: true,
      hasIcon: true,
    );
  }

  Widget buildColapsedTile(List<TileInfo> tile_infos_list) {
    if(tile_infos_list.length == 0){
      return Container();
    }
    TileInfo tile = tile_infos_list.elementAt(0);
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          Text("\n"+tile.heading, softWrap: true, overflow: TextOverflow.ellipsis,
            style: TextStyles.airbnbCerealMedium.copyWith(color: Colors.black,fontSize:  MyTheme.body3),
          ),
          Text(tile.body,

            softWrap: true, maxLines: 3, overflow: TextOverflow.ellipsis,
            style: TextStyles.airbnbCerealBook.copyWith(color: Colors.black,fontSize:  MyTheme.body3),
          ),
        ],
      ),
    );
  }

   static Widget buildExpandedTiles(List<TileInfo> tile_infos_list) {
    return Column(

      crossAxisAlignment: CrossAxisAlignment.start,
      children: getItemViews(tile_infos_list),
    );
  }

  static List<Widget> getItemViews(List<TileInfo> tile_infos_list) {
    List<Widget> widgets = new List();
    for(var index=0;index<tile_infos_list.length;index++){
      widgets.add(
      new Column(
        crossAxisAlignment: CrossAxisAlignment.start,

          children: <Widget>[
          Text("\n"+tile_infos_list.elementAt(index).heading, softWrap: true,
        style: TextStyles.airbnbCerealMedium.copyWith(color: Colors.black,fontSize: MyTheme.body3),
        ),
        Text(tile_infos_list.elementAt(index).body,
        textAlign: TextAlign.left,

        softWrap: true,
        style: TextStyles.airbnbCerealBook.copyWith(color: Colors.black,fontSize:  MyTheme.body3),
        ),
        ],
      )
      );
    }
    return widgets;
  }

}

enum TileInfoType{
  family_and_maternity,
  healthcare,
  invalidity,
  occupation_injury,
  unemployment_insurance,
  social_security_benefits
}

class TileInfo{
  final String heading, body;

  TileInfo(this.heading, this.body);


}