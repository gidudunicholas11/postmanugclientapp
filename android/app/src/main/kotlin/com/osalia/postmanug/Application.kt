package com.osalia.postmanug

import com.lyokone.location.LocationPlugin
import io.flutter.app.FlutterApplication
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugins.GeneratedPluginRegistrant

class Application : FlutterApplication(), PluginRegistry.PluginRegistrantCallback {
    override  fun onCreate() {
        super.onCreate()
        LocationPlugin.setPluginRegistrant(this)
    }


    override fun registerWith(registry: PluginRegistry) {
        GeneratedPluginRegistrant.registerWith(registry)
    }
}